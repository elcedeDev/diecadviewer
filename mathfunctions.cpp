#include "mathfunctions.h"
#include "die_coord.h"
namespace math {

	double _generalTolerance = 0.01;

	double getTolerance() {
		return _generalTolerance;
	}

	inline bool closeto( double A, double B, double _tolerance )
	{
		return fabs( A - B ) < _tolerance;
	}

	bool closeto( const Die::Coord& A, const Die::Coord& B, double _tolerance )
	{
		return (A - B).norm() < _tolerance;
	}

	inline double interpolate( double A, double B, double C, double D, double V )
	{
    //	ASSERT( A<=V && V<=C );

		return (D-B)*( (V-A)/(C-A) ) + B;
	}


}

double strToDbl( const QString& S ) {
    bool	neg = false;
    int		after = 0;
    double	whole = 0.0;

    const wchar_t	*sp = S.toStdWString().c_str();

    while( *sp ) {

        if( *sp>=48 && *sp<=57 ) {
            if( after == 0 ) whole = (whole*10) + (*sp - 48);
            else {
                whole += (*sp - 48) / (double) (after);
                after *= 10;
            }
        } else if( *sp == '.' || *sp == ',' ) after = 10;
        else if( *sp == '-' ) neg = true;
        else if( *sp == ';' ) break;
        ++sp;
    }

    return whole * ( neg ? -1 : 1 );
}
