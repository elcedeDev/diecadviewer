#include "mathfunctions.h"
#include "die_coord.h"
#include "element.h"
#include "importACC.h"
#include <locale.h>


namespace fileIO {


	inline 
	void 
	transform_point(double x1,double y1,double xo,double yo,double wr,double xs, double ys, double& x2, double& y2)
	{
		Die::Coord		P(x1,y1);
		Die::rotate(P, Die::Coord(Die::Deg(wr)));
		P.x*=xs; P.y*=ys;

		x2=P.x+xo, y2=P.y+yo;
	}


		acc_file::acc_file( std::map< int, element* >& E ) : file( E )
	{
		ctab[0] = "C,";
		ctab[1] = "SUB,";
		ctab[2] = "END,";
		ctab[3] = "Line";
		ctab[4] = "Arc";
		ctab[5] = "Tool";
		ctab[6] = "Macro";
		ctab[7] = "UI";
		ctab[8] = "%Customer";
		ctab[9] = "%Designer";
		ctab[10] = "%DeliveryDate";
		ctab[11] = "%Wood";
		bMirrorX = false;
		bMirrorY = false;
        m_currentTool = 0;
	}

    void acc_file::Load( const QString& fileName )
	{
		FILE			*fp = NULL;
        if (_wfopen_s(&fp, fileName.toStdWString().c_str(), L"r")==0) {
			read_acc(fp);
			fclose(fp);
        }
	}

    double acc_file::calcLength(int elemType, Die::Coord _sp, Die::Coord _ep, Die::Coord _cp, double angle )
	{


        double length = 0.0;

        //		double x1, x2, y1, y2, m1, m2, degree;
                double radius;

                if (elemType == element::ET_LINE){
                    length += sqrt(((_sp.x - _ep.x)*(_sp.x - _ep.x)) + ((_sp.y - _ep.y)*(_sp.y - _ep.y)));

                }
                else if (elemType == element::ET_ARC){

                    radius = (_sp - _cp).norm();

                    length += abs(radius * angle * (Die_pi / 180));



                }


                return length;



	}
	void acc_file::load_element(int et, double sx, double sy,
										double ex, double ey, 
										double cx, double cy,
                                        double rad, double toolnum, char *marco,
                                        double z, int linenum)
	{
		Die::Coord	start, end, center;
		start.x = sx;
		start.y = sy;
		end.x = ex;
		end.y = ey;
		center.x = cx;
		center.y = cy;

		element	*E = NULL;

			switch (et) {
				case 1: //LINE
					E = new element();
					E->setType( element::ET_LINE );
					E->setCoord( element::CID_START, start );
					E->setCoord( element::CID_END, end ); 
                    E->setLength(calcLength(element::ET_LINE, start, end, Die::Coord(0, 0),0));
                    E->setInt(element::IID_TOOL, m_currentTool);
                    E->setDouble(element::DID_ZCOORD, z);
                    E->setInt(element::IID_LINENUM, linenum);
					break;
				case 2: //ARC
                    E = new element();
                    E->setType(element::ET_ARC);

                    E->setCoord(element::CID_START, start);
                    E->setCoord(element::CID_END, end);
                    E->setDouble(element::DID_ARCANGLE, rad);
                    E->setCoord(element::CID_CENTER, center);
                    E->setLength(calcLength(element::ET_ARC, start, end, center, rad));
                    E->setInt(element::IID_TOOL, m_currentTool);
                    E->setDouble(element::DID_ZCOORD, z);
                    E->setInt(element::IID_LINENUM, linenum);

                    break;

				case 3: // tool
					E = new element();
					E->setType( element::ET_TOOL );
					E->setInt( element::IID_TOOL, (int) toolnum);
                    E->setInt(element::IID_LINENUM, linenum);

                    m_currentTool = (int) toolnum;

					break;
				case 4: // macro
					E = new element();
					E->setType( element::ET_MACRO );
                    E->setText( element::SID_MACRO, QString(marco));
                    E->setInt(element::IID_LINENUM, linenum);


					break;
			}
			if( E ) {
                E->setDouble( element::DID_POINTAGE, 1.0 );
                E->setInt(element::IID_ID, (int) _elements.size());
				_elements[ (int) _elements.size() ] = E; 
			}

	}

	int acc_file::read_acc(FILE *fp)
	{

		char line[str_length];			/* current line to parse                      */
		char helpline[str_length];		/* token to convert                           */
		int  Operator;					/* current operator                           */
		double token[30];				/* float token array. See doku...             */
        //bool move = true;

		int   i, j, t;					/* loop index in line                         */
		double startX = 0, startY = 0;
		double endX, endY;
        double arcAngle, arcRadian, arcCPX, arcCPY;
        double direction = 1.0;
		_locale_t		L = _create_locale(LC_ALL, "en-US");


		cindex = 0;                               /* always start with 0          */
		arrow_height = 2.0;
		int linecount = 0;
		rewind(fp);
		while ( fgets(line, 255, fp) != NULL )
		{
			linecount++;
            LeftTrim(line);
			for ( i=0, Operator=ACC_NONE; i<ACC_MAX; i++ )
				if ( strncmp(line, ctab[i], strlen(ctab[i])) == 0 ) 
					Operator = i;

			if ( Operator != ACC_NONE )
			{
				for ( i=0; i<30; i++ ) token[i] = 0.0;
				for ( i=0,j=0,t=0; i<=(int)strlen(line); i++ )
				{ 
					if ( line[i] == ','  || line[i] == '\r' || 
						line[i] == '\n' || line[i] == '\f' ||
						line[i] == '\t' || line[i] == '\0'  || line[i] == '(' || line[i] == ')')
					{
						helpline[j] = '\0';
						
						token[t++] = _atof_l(helpline, L);
						j = 0;
					}
					else
						helpline[j++] = line[i];
				}
			}


			switch ( Operator ) {

					case ACC_Line :                            /* line */
                        //if(move){
                        //    startX = token[2]*1000;
                        //    startY = token[3]*1000;
                        //}
                        //else{
							load_element( 1, startX, startY, token[2]*1000, token[3]*1000,
                                    0.0,0.0,0.0,0.0,0,token[4]*1000, linecount);
									startX = token[2]*1000;
									startY = token[3]*1000;
                       //}
							break;

						case ACC_Arc :                       
                            //arcRadius = token[7]  * token[8];
                            arcAngle = token[8]  ;//* token[8];
                            direction = 1.0;
                            if(token[7] < 0) direction = -1;

                            arcAngle *= direction;
                            //arcRadian = arcRadius * Die_pi / 180;
                            arcRadian = arcAngle * Die_pi / 180;

							arcCPX = token[2] * 1000;
							arcCPY = token[3] * 1000;
							endX = arcCPX + (cos(arcRadian)*(startX  - arcCPX) - sin(arcRadian) * (startY  - arcCPY));
							endY = arcCPY + (sin(arcRadian)*(startX  - arcCPX) + cos(arcRadian) * (startY  - arcCPY));
							if (endX <= 0.0001 && endX >= -0.0001)
								endX = 0;
							if (endY <= 0.0001 && endY >= -0.0001)
								endY = 0;

							load_element( 2,  
                                    startX, startY, endX, endY,     /* sx, sy, ex, ey    */
                                    arcCPX, arcCPY, arcAngle,      /* cx, cy, rad,      */
                                    0.0,0,token[4]*1000, linecount);
                            startX = endX;
                            startY = endY;

							break;
					case ACC_Tool :
                        //if(int(token[1]) == 0){
                        //       move = true;
                        //}
                        //else{
                        //	move = false;
							load_element( 3, 0.0, 0.0, 0.0, 0.0,
                                          0.0, 0.0, 0.0,token[1],0,0.0,linecount );
                        //}



						break;
					case ACC_Macro :
                            char macro[1024];
							char temp[100];
                            sprintf_s(temp,sizeof(macro),"%f",token[1]);
                            strcpy_s(macro,temp);
                            strcat_s(macro,",");
                            sprintf_s(temp,sizeof(macro),"%f",token[2]);
                            strcat_s(macro,temp);
                            strcat_s(macro,",");
                            sprintf_s(temp,sizeof(macro),"%f",token[3]);
                            strcat_s(macro,temp);
                            load_element( 4, 0.0, 0.0, 0.0, 0.0,
                                          0.0, 0.0, 0.0,0,macro,0.0,linecount);

						break;

					default : break;
			}
        }


		_free_locale(L);
		//MessageBox(0,_T("Under Construction"),_T("MeasureMate"),0);
		return(0);
	}
}
