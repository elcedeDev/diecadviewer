#pragma once
#include <QString>

namespace Die {
	class Coord;
}

namespace math {

    extern  double _generalTolerance;
    extern  double getTolerance();
    extern  inline bool closeto( double, double, double );
    extern  bool closeto( const Die::Coord&, const Die::Coord&, double );

    extern  inline double interpolate( double, double, double, double, double );
}

double strToDbl( const QString& );
//double strToDbl( const TCHAR* );

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif
