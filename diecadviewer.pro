TEMPLATE = lib
TARGET = DieCadViewer
QT += declarative
CONFIG += qt plugin c++11


DESTDIR = components/DieCadViewer
TARGET = $$qtLibraryTarget($$TARGET)
uri = DieCadViewer

RC_FILE = proj.rc

# Input
SOURCES += \
    diecadviewer_plugin.cpp \
    diecadviewer.cpp \
    element.cpp \
    ImportACC.cpp \
    mathfunctions.cpp \
    ImportCNC.cpp \
    ImportHPGL.cpp

HEADERS += \
    diecadviewer_plugin.h \
    diecadviewer.h \
    element.h \
    die_coord.hpp \
    die_coord.h \
    die_bounds.hpp \
    die_bounds.h \
    mathfunctions.h \
    ImportACC.h \
    Import.h \
    ImportCNC.h \
    ImportHPGL.h \
    version.h \
    textfielddoublevalidator.h

DISTFILES = qmldir

!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
    copy_qmldir.target = $$OUT_PWD/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
    copy_qmldir.commands = $(COPY_FILE) \"$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)\"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}

qmldir.files = qmldir
unix {
    installPath = $$[QT_INSTALL_QML]/$$replace(uri, \\., /)
    qmldir.path = $$installPath
    target.path = $$installPath
    INSTALLS += target qmldir
}

OTHER_FILES += \
    proj.rc
