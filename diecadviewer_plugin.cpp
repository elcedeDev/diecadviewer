#include "diecadviewer_plugin.h"
#include "diecadviewer.h"

#include "textfielddoublevalidator.h"

#include <qdeclarative.h>
#include <QtPlugin>


void DieCadViewerPlugin::registerTypes(const char *uri)
{
    // @uri elcede.qmlcomponents
    qmlRegisterType<DieCadViewer>(uri, 1, 0, "DieCadViewer");
    qmlRegisterType<TextFieldDoubleValidator>(uri, 1, 0, "TextFieldDoubleValidator");
}

