#include "stdafx.h"
#include "ProgressWnd.h"

#include "die_coord.h"
#include "element.h"
#include "importCNC.h"
#include <locale.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define pi 3.14159265


namespace fileIO
{

	cnc_file::cnc_file(std::map< int, element* >& E, bool  mirrorX, bool mirrorY, bool displayProgressWnd, bool bShowMsgDlg) : file(E, bShowMsgDlg)
	{
		bMirrorX = mirrorX;
		bMirrorY = mirrorY;
		L = _create_locale(LC_ALL, "en-US");
		m_lastOperation = currOperation::line;
	}

	cnc_file::~cnc_file()
	{

		_free_locale(L);
	}

	void cnc_file::Load(const CString& fileName, bool displayProgressWnd)
	{
		FILE			*fp = NULL;
		if (_wfopen_s(&fp, fileName, L"r") == 0) {
			read_cnc(fp, fileName, displayProgressWnd);
			fclose(fp);
		}
	}

	int cnc_file::read_cnc(FILE *fp, CString fileName, bool displayProgressWnd)
	{
		char line[255];
		bool isRelativeCoord = false;
		bool isImperialSystem = false;
		Die::Coord currPos(0, 0);
		Die::Coord endPos;
		Die::Coord centerPoint;
		bool isCCW = true;
		int currOperation = currOperation::line;
		rewind(fp);
		int currTool = 0;
		int et;
		bool isXEndSet = false;
		bool isXCenterSet = false;
		int lineCount = 0;

		rewind(fp);


		CProgressWnd wndProgress(NULL, _T("Progress"));

		if (displayProgressWnd) {
			while (fgets(line, 255, fp) != NULL)
				++lineCount;
			wndProgress.Display();
			wndProgress.SetRange(0, lineCount * 100, 100);
			CString smsg;
			smsg.Format(_T("Loading file %s...\n\n"), fileName);
			wndProgress.SetText(smsg);

			rewind(fp);
		}
		while (fgets(line, 255, fp) != NULL)
		{
			if (displayProgressWnd) {
				wndProgress.StepIt();
				wndProgress.PeekAndPump();
			}

			CString l(line);

			if (l.Find(_T("Q1")) != -1) {
				int t = 0;
				t = t;
			}
			std::vector<CString> tokens = getTokens(l.MakeUpper());

			et = -1;
			isXEndSet = false;
			isXCenterSet = false;

			for (auto it = tokens.begin(); it != tokens.end(); ++it) {


				if ((*it).Left(1) == _T("N")) {

					continue;

				}
				else if ((*it).Left(1) == _T("G")) {
					int param = _wtoi_l((*it).Mid(1), L);


					switch (param) {
					case 0: currOperation = currOperation::move; break;
					case 1:  currTool ? currOperation = currOperation::line : currOperation::move; m_lastOperation = currOperation::line; break;
					case 2:  currOperation = currOperation::arcCW; break; 
					case 3:  currOperation = currOperation::arcCCW; break; 
					case 20: isImperialSystem = true; break;
					case 21: isImperialSystem = false; break;
					case 70: isImperialSystem = true; break;
					case 71: isImperialSystem = false; break;
					case 90: isRelativeCoord = false; break;
					case 91: isRelativeCoord = true; break;
					case 92: break;
					default: {
						if (_bShowMsgDlg) {
							CString tmp;
							tmp.Format(_T("G code %d is not handled"), param);
							AfxMessageBox(tmp);
						}
						}
					}

				}
				else if ((*it).Left(1) == _T("X")) {

					double param = _wtof_l((*it).Mid(1), L);

					if (isImperialSystem)
						param *= 25.4;

					switch (currOperation)
					{
					case currOperation::move: {
						if (isRelativeCoord)
							currPos.x += param;
						else
							currPos.x = param;

						endPos = currPos;
						break;
					}
					case currOperation::line:
					{
						et = 1;
						endPos = currPos;
						if (isRelativeCoord) {
							endPos.x = currPos.x + param;
							isXEndSet = true;
						}
						else
							endPos.x = param;

						break;
					}
					case currOperation::arcCW:
					{
						et = 2;
						endPos = currPos;
						if (isRelativeCoord) {
							endPos.x = currPos.x + param;
							isXEndSet = true;
						}
						else
							endPos.x = param;

						isCCW = false;
						break;
					}
					case currOperation::arcCCW:
					{
						et = 2;
						endPos = currPos;
						if (isRelativeCoord) {
							endPos.x = currPos.x + param;
							isXEndSet = true;
						}
						else
							endPos.x = param;

						isCCW = true;
						break;
					}

					default:
						break;
					}

				}
				else if ((*it).Left(1) == _T("Y")) {
					double param = _wtof_l((*it).Mid(1), L);

					if (isImperialSystem)
						param *= 25.4;

					switch (currOperation)
					{
					case currOperation::move: {
						if (isRelativeCoord)
							currPos.y = currPos.y + param;
						else
							currPos.y = param;
						endPos = currPos;
						break;
					}
					case currOperation::line:
					{
						et = 1;
						if (isRelativeCoord) {
							if (!isXEndSet)
								endPos.x = currPos.x;

							endPos.y = currPos.y + param;
						}
						else
							endPos.y = param;

						break;
					}
					case currOperation::arcCW:
					{
						et = 2;
						if (isRelativeCoord) {
							if (!isXEndSet)
								endPos.x = currPos.x;

							endPos.y = currPos.y + param;
						}
						else
							endPos.y = param;

						isCCW = false;
						break;
					}
					case currOperation::arcCCW:
					{
						et = 2;
						if (isRelativeCoord) {
							if (!isXEndSet)
								endPos.x = currPos.x;

							endPos.y = currPos.y + param;
						}
						else
							endPos.y = param;

						isCCW = true;
						break;
					}

					default:
						break;
					}


				}
				else if ((*it).Left(1) == _T("T")) {

					currTool = _wtoi_l((*it).Mid(1), L);
					if (!currTool) {
						m_lastOperation = currOperation;
						currOperation = currOperation::move;
					}
					else {
						currOperation = m_lastOperation;
					}
					
				}
				else if ((*it).Left(1) == _T("M")) {
					if ((*it).Left(2) == _T("M5"))
						currTool = _wtoi_l((*it).Mid(2), L);
					else
						continue;
				}
				else if ((*it).Left(1) == _T("(") || (*it).Left(1) == _T("{")) {
					break;


				}
				else if ((*it).Left(1) == _T("$")) {
					break;


				}
				else if ((*it).Left(1) == _T("Q")) {
				break;


				}
				else if ((*it).Left(1) == _T("%")) {
					continue;


				}
				else if ((*it).Left(1) == _T("I")) {
					double param = _wtof_l((*it).Mid(1), L);

					if (isImperialSystem)
						param *= 25.4;

					switch (currOperation)
					{
					case currOperation::arcCW:
					{
						et = 2;
						centerPoint = currPos;
						
						centerPoint.x = currPos.x + param;
						isXCenterSet = true;
						

						isCCW = false;
						break;
					}
					case currOperation::arcCCW:
					{
						et = 2;
						centerPoint = currPos;
						
						centerPoint.x = currPos.x + param;
						isXCenterSet = true;
						

						isCCW = true;
						break;
					}

					default:
						break;
					}


				}
				else if ((*it).Left(1) == _T("J")) {
					double param = _wtof_l((*it).Mid(1), L);

					if (isImperialSystem)
						param *= 25.4;

					switch (currOperation)
					{
					case currOperation::arcCW:
					{
						et = 2;
						if (!isXCenterSet)
							centerPoint.x = currPos.x;

						centerPoint.y = currPos.y + param;
						

						isCCW = false;
						break;
					}
					case currOperation::arcCCW:
					{
						et = 2;
						if (!isXCenterSet)
							centerPoint.x = currPos.x;

						centerPoint.y = currPos.y + param;
						

						isCCW = true;
						break;
					}

					default:
						break;
					}


				}
				else if ((*it).Left(1) == _T("F")) {
					continue;


				}
				else {
					if(_bShowMsgDlg)
						AfxMessageBox(*it);
				}

			}

			if (et > 0) {

				double rad = 0;

				if (et == 2) {
					Die::FreeAngle SwA;
					Die::Angle StA(currPos - centerPoint);
					Die::Angle EnA(endPos - centerPoint);

					if (math::closeto(StA.degree(), EnA.degree(), math::getTolerance()))
						SwA = Die::Rad(Die_2pi);
					else
						if (StA.radian() < EnA.radian())
							SwA = EnA - StA;
						else
							SwA = Die::Rad(Die_2pi) - (StA - EnA);

					rad = SwA.degree();

					if (!isCCW && !math::closeto(rad,360, 0.01))
						rad -= 360;

				}

				load_element(et, currPos.x, currPos.y, endPos.x, endPos.y, centerPoint.x, centerPoint.y, rad, currTool);
				currPos = endPos;
			}
			
		}
		return 0;
	}

	std::vector<CString> cnc_file::getTokens(CString line)
	{
		std::vector<CString> ret;
		int   i, j;
		char helpline[255];
		bool inComment = false;

		for (i = 0, j = 0; i <= (int)line.GetLength(); i++)
		{
			if (!inComment && j && (line[i] == '$' || line[i] == '\r' ||
				line[i] == '\n' || line[i] == '%' || line[i] == 'T' ||  line[i] == 'I' || line[i] == 'J' || line[i] == 'M' ||
				line[i] == 'N' || line[i] == 'G' || line[i] == 'X' ||  line[i] == 'Y' || line[i] == '\0' || line[i] == 'F' || line[i] == '(' || line[i] == ')' || line[i] == '{' || line[i] == '}'))
			{
				if (line[i] == '(' || line[i] == '{') {
					inComment = true;
				}
				
				if (line[i] == ')' || line[i] == '}') {
					helpline[j++] = line[i];
				}

				helpline[j] = '\0';
				CString tmp(helpline);
				if(tmp != _T("\n"))
					ret.push_back(tmp);

				j = 0;
				helpline[j] = '\0';
				if (line[i] != ')' || line[i] == '}') {
					helpline[j++] = line[i];
				}
			}
			else {
				helpline[j++] = line[i];
				if (i + 1 <= line.GetLength() && (line[i + 1] == ')' || line[i] == '}'))
					inComment = false;
			}
		}


		return ret;
	}

	void cnc_file::load_element(int et, double sx, double sy,
		double ex, double ey,
		double cx, double cy,
		double rad, double toolnum)
	{
		Die::Coord	start, end, center;
		start.x = sx;
		start.y = sy;
		end.x = ex;
		end.y = ey;
		center.x = cx;
		center.y = cy;

		element	*E = NULL;

		switch (et) {
			case 1: //LINE
				E = new element();
				E->setType(element::ET_LINE);
				E->setCoord(element::CID_START, start);
				E->setCoord(element::CID_END, end);
				E->setLength(calcLength(element::ET_LINE, start, end, Die::Coord(0, 0), 0));
				E->setInt(element::IID_TOOL, toolnum);

				break;
			case 2: //ARC
			{
				E = new element();
				E->setType(element::ET_ARC);


				E->setCoord(element::CID_START, start);
				E->setCoord(element::CID_END, end);
				E->setDouble(element::DID_ARCANGLE, rad);
				E->setCoord(element::CID_CENTER, center);
				E->setLength(calcLength(element::ET_ARC, start, end, center, rad));
				E->setInt(element::IID_TOOL, toolnum);

				break;
			}
		}
		if (E) {
			E->setDouble(element::DID_POINTAGE, 1.0);
			E->setDouble(element::DID_BRIDGELENGTH, 0.0);
			E->setInt(element::IID_BRIDGENUM, 0);
			_elements[(int)_elements.size()] = E;
		}

		
	}


	double cnc_file::calcLength(int elemType, Die::Coord _sp, Die::Coord _ep, Die::Coord _cp, double angle)
	{

		double length = 0.0;

		//		double x1, x2, y1, y2, m1, m2, degree;
		double radius;

		if (elemType == element::ET_LINE) {
			length += sqrt(((_sp.x - _ep.x)*(_sp.x - _ep.x)) + ((_sp.y - _ep.y)*(_sp.y - _ep.y)));

		}
		else if (elemType == element::ET_ARC) {

			radius = (_sp - _cp).norm();

			length += abs(radius * angle * (pi / 180));



		}


		return length;


	}
};