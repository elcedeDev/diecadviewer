#ifndef DIECADVIEWER_H
#define DIECADVIEWER_H

#include <QObject>
#include <QDeclarativeItem>
#include <QColor>
#include <QBitArray>
#include <QPainterPath>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QFileSystemWatcher>
#include <QGestureEvent>
#include <QPen>
#include <QProcess>


class QGestureEvent;
class QPanGesture;
class QPinchGesture;
class QSwipeGesture;


class element;
namespace Die {
    class Coord;
    class Angle;
}

//this version is compatible with VV8 1.3.8 - 12

class DieCadViewer : public QDeclarativeItem
{
    Q_OBJECT
    Q_DISABLE_COPY(DieCadViewer)
    Q_PROPERTY(QString filename READ filename WRITE setFileName NOTIFY filenameChanged)
    Q_PROPERTY(QColor toolColor READ toolColor WRITE setToolColor)
    Q_PROPERTY(int toolWidth READ toolWidth WRITE setToolWidth)
    Q_PROPERTY(qreal toolPathLength READ toolPathLength) // WRITE setToolPathLength)
    Q_PROPERTY(bool toolShow READ toolShow WRITE setToolShow)
    Q_PROPERTY(int toolIndex READ toolIndex WRITE setToolIndex)
    Q_PROPERTY(bool directionShow READ directionShow WRITE setDirectionShow)

    Q_PROPERTY(QString totalMovesName READ totalMovesName WRITE setTotalMovesName)
    Q_PROPERTY(QString emptyMovesName READ emptyMovesName WRITE setEmptyMovesName)
    Q_PROPERTY(QString movesDimension READ movesDimension WRITE setMovesDimension)
    Q_PROPERTY(qreal movesFactor READ movesFactor WRITE setMovesFactor)
    Q_PROPERTY(qreal scaleFactor READ scaleFactor WRITE setScaleFactor)

    Q_PROPERTY(QPointF position READ position WRITE setPosition)

    Q_PROPERTY(int actualElem READ actualElem WRITE setActualElem)
    Q_PROPERTY(int selectedElem READ selectedElem WRITE setSelectedElem)

    Q_PROPERTY(QColor textColor READ textColor WRITE setTextColor)
    Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
    Q_PROPERTY(QColor processedColor READ processedColor WRITE setProcessedColor)
    Q_PROPERTY(QString fileErrorText READ fileErrorText WRITE setFileErrorText)

    Q_PROPERTY(qreal minX READ minX WRITE setMinX)
    Q_PROPERTY(qreal minY READ minY WRITE setMinY)
    Q_PROPERTY(qreal maxX READ maxX WRITE setMaxX)
    Q_PROPERTY(qreal maxY READ maxY WRITE setMaxY)

    //Q_PROPERTY(QPointF zeroPosition READ zeroPosition WRITE setZeroPosition NOTIFY zeroPositionChanged)
    Q_PROPERTY(QPointF zeroPosition_In READ zeroPosition_In WRITE setZeroPosition_In)
    Q_PROPERTY(QPointF zeroPosition_Out READ zeroPosition_Out NOTIFY zeroPositionChanged)
    Q_PROPERTY(QPointF machinePosition READ machinePosition WRITE setMachinePosition NOTIFY machinePositionChanged)

    Q_PROPERTY(QSizeF machineSize READ machineSize WRITE setMachineSize)
    Q_PROPERTY(bool showMachine READ showMachine WRITE setShowMachine)

    Q_PROPERTY(int engravingTool READ engravingTool WRITE setEngravingTool)
    Q_PROPERTY(bool isEngraving READ isEngraving )

    Q_PROPERTY(bool isJobMoveAllowed READ isJobMoveAllowed WRITE setisJobMoveAllowed )

    Q_PROPERTY(qreal rotationAngle READ rotationAngle WRITE setRotationAngle NOTIFY rotationAngleChanged)
    Q_PROPERTY(QPointF rotationCenter READ rotationCenter WRITE setRotationCenter NOTIFY rotationCenterChanged)
    Q_PROPERTY(bool isRotationUsed READ isRotationUsed WRITE setisRotationUsed) // NOTIFY isRotationUsedChanged )
    Q_PROPERTY(QPointF offset READ offset WRITE setOffset NOTIFY offsetChanged)

    Q_PROPERTY(qreal flange0PosY READ flange0PosY WRITE setFlange0PosY NOTIFY flangePosYChanged)
    Q_PROPERTY(qreal flange1PosY READ flange1PosY WRITE setFlange1PosY NOTIFY flangePosYChanged)
    Q_PROPERTY(qreal flange2PosY READ flange2PosY WRITE setFlange2PosY NOTIFY flangePosYChanged)
    Q_PROPERTY(qreal flange3PosY READ flange3PosY WRITE setFlange3PosY NOTIFY flangePosYChanged)
    Q_PROPERTY(qreal flange4PosY READ flange4PosY WRITE setFlange4PosY NOTIFY flangePosYChanged)
    Q_PROPERTY(qreal flange5PosY READ flange5PosY WRITE setFlange5PosY NOTIFY flangePosYChanged)
    Q_PROPERTY(qreal flange6PosY READ flange6PosY WRITE setFlange6PosY NOTIFY flangePosYChanged)

    Q_PROPERTY(QColor flangeColor READ flangeColor WRITE setFlangeColor)
    Q_PROPERTY(bool showFlange READ showFlange WRITE setShowFlange)
    //this flag is used to switch on or off regurar file checking by deafult is false -- file is not checked
    //used in timerEvent and in loadFile
    Q_PROPERTY(bool isFileCheckedRegularly READ isFileCheckedRegularly WRITE isFileCheckedRegularly)
    //run windows command or exe
    Q_PROPERTY(QString strExecuteCmd READ strExecuteCmd WRITE setstrExecuteCmd)
    Q_PROPERTY(QString strExecuteCmdParameters READ strExecuteCmdParameters WRITE setstrExecuteCmdParameters)
    Q_PROPERTY(bool bStartExecuteCmd READ bStartExecuteCmd WRITE setbStartExecuteCmd)
    Q_PROPERTY(int  iExecuteCmdExitCode READ iExecuteCmdExitCode)
    //Q_PROPERTY(bool bKillExecuteCmd READ bKillExecuteCmd WRITE setbKillExecuteCmd NOTIFY bKillExecuteCmdChanged)

    Q_PROPERTY(int machineMargin READ machineMargin WRITE setMachineMargin) // NOTIFY machineMarginChanged)
    Q_PROPERTY(qreal flangeWidth READ flangeWidth WRITE setFlangeWidth) // NOTIFY flangeWidthChanged)
    Q_PROPERTY(int numofElem READ numofElem ) // NOTIFY machineMarginChanged)
    //these properties used by fileOpen dialog
    Q_PROPERTY(QString fileOpenDirectory READ fileOpenDirectory WRITE setfileOpenDirectory NOTIFY fileOpenDirectoryChanged)
    Q_PROPERTY(QString fileOpenSelectFile READ fileOpenSelectFile WRITE setfileOpenSelectFile NOTIFY fileOpenSelectFileChanged)
    Q_PROPERTY(QString fileOpenFilter READ fileOpenFilter WRITE setfileOpenFilter NOTIFY fileOpenFilterChanged)

    //Q_PROPERTY(bool visible READ isVisible WRITE setVisible  NOTIFY visibleChanged )

public:
    DieCadViewer(QDeclarativeItem *parent = 0);
    ~DieCadViewer();

    QString filename() const;
    Q_INVOKABLE void setFileName(const QString &filename);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    QColor toolColor() const;
    void setToolColor(const QColor&);

    int toolWidth() const;
    void setToolWidth(const int&);

    qreal toolPathLength() const;
    //void setToolPathLength(const int&)

    bool toolShow() const;
    void setToolShow(const bool);

    int toolIndex() const;
    void setToolIndex(int);

    bool directionShow() const;
    void setDirectionShow(const bool);

    Q_INVOKABLE void loadFile(QString fileName);
    Q_INVOKABLE void refresh();
    Q_INVOKABLE void reDraw();


    Q_INVOKABLE QString convertUrlToNativeFilePath(const QUrl& urlStylePath) const;


    QString totalMovesName() const;
    Q_INVOKABLE void setTotalMovesName(const QString &totalMovesName);

    QString emptyMovesName() const;
    Q_INVOKABLE void setEmptyMovesName(const QString &emptyMovesName);

    QString movesDimension() const;
    Q_INVOKABLE void setMovesDimension(const QString &movesDimension);

    qreal scaleFactor() const;
    Q_INVOKABLE void setScaleFactor(const qreal);

    QPointF position() const;
    Q_INVOKABLE void setPosition(const QPointF);

    qreal movesFactor() const;
    void setMovesFactor(const qreal);

    int selectedElem() const;
    Q_INVOKABLE void setSelectedElem(int);

    int actualElem() const;
    Q_INVOKABLE void setActualElem(int);

    QColor textColor() const;
    Q_INVOKABLE void setTextColor(const QColor&);

    QColor backgroundColor() const;
    Q_INVOKABLE void setBackgroundColor(const QColor&);

    QColor processedColor() const;
    Q_INVOKABLE void setProcessedColor(const QColor&);

    QString fileErrorText() const;
    Q_INVOKABLE void setFileErrorText(const QString&);

    qreal minX() const;
    void setMinX(const qreal);

    qreal minY() const;
    void setMinY(const qreal);

    qreal maxX() const;
    void setMaxX(const qreal);

    qreal maxY() const;
    void setMaxY(const qreal);

    QPointF zeroPosition_In() const;
    Q_INVOKABLE void setZeroPosition_In(const QPointF);

    QPointF zeroPosition_Out() const;

    QPointF machinePosition() const;
    Q_INVOKABLE void setMachinePosition(const QPointF);

    QSizeF machineSize() const;
    Q_INVOKABLE void setMachineSize(const QSizeF);

    bool showMachine() const;
    Q_INVOKABLE void setShowMachine(const bool);

    int engravingTool() const;
    Q_INVOKABLE void setEngravingTool(const int);

    bool isEngraving() const;

    bool isJobMoveAllowed();
    Q_INVOKABLE void setisJobMoveAllowed(const bool);

    qreal rotationAngle() const;
    void setRotationAngle(const qreal);

    QPointF rotationCenter() const;
    void setRotationCenter(const QPointF);

    void grabGestures(const QList<Qt::GestureType> &gestures);
    bool gestureEvent(QGestureEvent *event);

    bool isRotationUsed() const;
    Q_INVOKABLE void setisRotationUsed(const bool);

    QPointF offset() const;
    void setOffset(const QPointF);

    qreal flange0PosY() const{return m_flangePosY[0];}
    void setFlange0PosY(const qreal &p){ m_flangePosY[0] = p;}
    qreal flange1PosY() const{return m_flangePosY[1];}
    void setFlange1PosY(const qreal &p){ m_flangePosY[1] = p;}
    qreal flange2PosY() const{return m_flangePosY[2];}
    void setFlange2PosY(const qreal &p){ m_flangePosY[2] = p;}
    qreal flange3PosY() const{return m_flangePosY[3];}
    void setFlange3PosY(const qreal &p){ m_flangePosY[3] = p;}
    qreal flange4PosY() const{return m_flangePosY[4];}
    void setFlange4PosY(const qreal &p){ m_flangePosY[4] = p;}
    qreal flange5PosY() const{return m_flangePosY[5];}
    void setFlange5PosY(const qreal &p){ m_flangePosY[5] = p;}
    qreal flange6PosY() const{return m_flangePosY[6];}
    void setFlange6PosY(const qreal &p){ m_flangePosY[6] = p;}

    QColor flangeColor() const{return m_flangeColor;}
    void setFlangeColor(const QColor &c){ m_flangeColor = c;}

    bool showFlange() const{return m_showFlange;}
    Q_INVOKABLE void setShowFlange(const bool b){ m_showFlange = b;}

    bool isFileCheckedRegularly() const{return m_isFileCheckedRegularly;}
    Q_INVOKABLE void isFileCheckedRegularly(const bool b){ m_isFileCheckedRegularly = b;}

    Q_INVOKABLE void zoomAll();

    QString strExecuteCmd() const{return m_strExecuteCmd;}
    Q_INVOKABLE void setstrExecuteCmd(const QString &strExecuteCmd){ m_strExecuteCmd = strExecuteCmd;}

    QString strExecuteCmdParameters() const{return m_strExecuteCmdParameters;}
    Q_INVOKABLE void setstrExecuteCmdParameters(const QString &strExecuteCmdParameters){ m_strExecuteCmdParameters = strExecuteCmdParameters;}

    bool bStartExecuteCmd() const{return m_bStartExecuteCmd;}
    Q_INVOKABLE void setbStartExecuteCmd(const bool b){ m_bStartExecuteCmd = b;}

    int iExecuteCmdExitCode() const{return m_iExecuteCmdExitCode;}

    Q_INVOKABLE void killExecuteCmd(){ if(m_process_ptr != NULL) m_process_ptr->kill();}

    int machineMargin() const{return m_machineMargin;}
    Q_INVOKABLE void setMachineMargin(int m) { m_machineMargin = m;}

    qreal flangeWidth() const{return m_flangeWidth;}
    Q_INVOKABLE void setFlangeWidth(qreal w){m_flangeWidth = w;}

    //returns the num of element
    int numofElem() const{return m_numofElem;}

    QString fileOpenDirectory() const{return m_fileOpenDirectory;}
    Q_INVOKABLE void setfileOpenDirectory(const QString &str) {m_fileOpenDirectory = str;}
    QString fileOpenSelectFile() const{return m_fileOpenSelectFile;}
    Q_INVOKABLE void setfileOpenSelectFile(const QString &str) {m_fileOpenSelectFile = str;}
    QString fileOpenFilter() const{return m_fileOpenFilter;}
    Q_INVOKABLE void setfileOpenFilter(const QString &str) {m_fileOpenFilter = str;}

    //Q_INVOKABLE int selectElem(int id);

    Q_INVOKABLE bool fileCopy(QString from, QString to);
    Q_INVOKABLE QString fileOpen(QObject*);

    //virtual bool visible() const{return  QGraphicsItem::isVisible();}
    //virtual void setVisible(bool b)  {  QGraphicsItem::setVisible(b);  emit visibleChanged(); }

signals:
    void filenameChanged();
    void fileLoaded();
    void zeroPositionChanged();
    void machinePositionChanged();
    void rotationAngleChanged();
    void rotationCenterChanged();
    //void isRotationUsedChanged();
    void offsetChanged();
    void flangePosYChanged();
    void executeCmdFinished();
    //void visibleChanged();
    void fileOpenDirectoryChanged();
    void fileOpenSelectFileChanged();
    void fileOpenFilterChanged();

public slots:
    void fileModified(const QString& str);
    void executeCmdEnd(int, QProcess::ExitStatus);
    void executeCmdStarted();


protected:
    virtual void wheelEvent(QGraphicsSceneWheelEvent *event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent * event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent * event);
    void         timerEvent(QTimerEvent *event);

    virtual bool         event(QEvent *event);

    //virtual QRectF 	boundingRect();

private:

    struct tool{
        QColor color;
        int    width;
        qreal  pathLength;
    };

    QString                         m_filename;
    QString                         m_nativefilename;
    std::map< int, element* >		m_elements;
    QRectF                          m_boundingBox;
    qreal                           m_Zoom;

    int                             m_machineMargin;

    QRectF                          m_jobRect;

    QMap<int, tool>                 m_tools;
    //QMap<int, QColor>               m_toolColors;
    QBitArray                       m_toolShow;
    int                             m_toolIndex;
    bool                            m_directionShow;
    bool                            m_isLoaded;

    QPointF                         m_offset;
    QPointF                         m_mousepos;
    int                             m_textMargin;

    bool                            m_drag;

    qreal                           m_totalMoves;
    qreal                           m_emptyMoves;

    QString                         m_totalMovesName;
    QString                         m_emptyMovesName;
    QString                         m_movesDimension;

    qreal                           m_movesFactor;

    QColor                          m_selectedColor;
    QColor                          m_processedColor;

    int                             m_selectedElement;
    int                             m_actualElement;

    QColor                          m_textColor;
    QColor                          m_backgroundColor;
    QString                         m_fileErrorText;
    QString                         m_fileLoadingText;

    QPointF                         m_min;
    QPointF                         m_max;

    qint16                          m_cheksumBegin;
    qint16                          m_cheksumEnd;
    bool                            m_bNeedRefresh;
    bool                            m_isFileChanged; //used in checkfile()


    int                             m_timer;

    QFileSystemWatcher              m_watcher;
    bool                            m_isFileModified; // used in fileModified()

    bool                            m_verbose;

    QTime                           m_tapTime;
    int                             m_tapDoubleClickTime;

    qreal                           m_rotationAngle;
    QPointF                         m_rotationCenter;


    qreal                           m_machineXlength;
    qreal                           m_machineYlength;
    QSizeF                          m_machineSize;

    QPointF                         m_zeroPoint;
    QPointF                         m_zeroPosition_In;  //in coming values
    QPointF                         m_zeroPosition_Out; //out going values

    QPointF                         m_machinePosition;
    QRectF                          m_machineRect;
    QPen                            m_machinePen;
    QPointF                         m_machine_ZeroPoint; //when m_showMachine == true
    QPointF                         m_machine_Offset;    //when m_showMachine == true
    qreal                           m_machine_Zoom;      //when m_showMachine == true
    QPointF                         m_normal_ZeroPoint;  //when m_showMachine == false
    QPointF                         m_normal_Offset;     //when m_showMachine == false
    qreal                           m_normal_Zoom;       //when m_showMachine == false

    bool                            m_showMachine;
    QTime                           m_zeroPositionLastModified;

    int                             m_engravingTool;
    bool                            m_IsEngraving;
    bool                            m_isJobMoveAllowed;
    bool                            m_isRotationUsed;

    qreal                           m_flangePosY[7];
    QColor                          m_flangeColor;
    bool                            m_showFlange;
    qreal                           m_flangeWidth;
    int                             m_numofElem;

    QString                         m_fileOpenDirectory;
    QString                         m_fileOpenSelectFile;
    QString                         m_fileOpenFilter;

    bool                            m_isFileCheckedRegularly;
    bool                            m_isRotaMate;

    QString                         m_strExecuteCmd;
    QString                         m_strExecuteCmdParameters;
    bool                            m_bStartExecuteCmd;
    int                             m_iExecuteCmdExitCode;
    //bool                            m_bKillExecuteCmd;

    QProcess*                       m_process_ptr;

    QPainterPath                    m_machineHead;
    QPainterPath                    m_arrowDirection; //arrow template to show line and arc direction
    QMap<int, QPair<int, QPainterPath>>   m_elementPath;// separate path for tools QPair<toolNum, QPainterPath>

    void drawArrow(element*, QPainter*);

    void Arc(element* e, QPainter* painter );

    void selectElem(QPointF&);
    void outStr(QString &str);       //formats selected element output string
    QPointF S2C(const QPointF&);


    void checkFile();

    void pinchTriggered(QPinchGesture*, QGestureEvent*);
    void panTriggered(QPanGesture*);
    void tapTriggered(QTapGesture*);

    void drawMachinePosition(QPainter*);

    void scalePLTfile();
    double calcLength(int elemType, Die::Coord _sp, Die::Coord _ep, Die::Coord _cp);

    bool executeCmd();
};



#endif // DIECADVIEWER_H
