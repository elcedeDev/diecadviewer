#pragma once

#include "mathfunctions.h"

namespace Die
{

	Bounds::Bounds()
	: m_empty(true)
	{}


	Bounds::Bounds(const Coord& P, const Coord& Q)
	: m_empty(true)
	{
		Die::extend(*this,P),  Die::extend(*this,Q);
	}


	void
	Bounds::clear()
	{ 
		m_empty=true; 
	}


	Coord
	Bounds::lbound() const
	{ 
		return (empty() ? Coord() : m_lbound);
	}


	Coord
	Bounds::hbound() const
	{ 
		return (empty() ? Coord() : m_hbound); 
	}


	Bounds
	Bounds::extend(const Coord& P) const
	{
		Bounds B=*this;
		Die::extend(B, P);
		return B;
	}


	Bounds
	Bounds::extend(const Bounds& BX) const
	{
		Bounds B=*this;
		Die::extend(B, BX);
		return B;
	}


	Bounds
	Bounds::intersect(const Bounds& BI) const
	{
		Bounds B=*this;
		Die::intersect(B, BI);
		return B;
	}


	Bounds
	Bounds::grow(const double& G) const
	{
		Bounds B=*this;
		Die::grow(B, G);
		return B;
	}


	bool
	Bounds::empty() const
	{ 
		return m_empty; 
	}


	bool
	Bounds::contains(const Coord& P) const 
	{
		return (empty() ? false : (m_lbound.x<=P.x && P.x<=m_hbound.x && m_lbound.y<=P.y && P.y<=m_hbound.y));
	}


	bool
	Bounds::strictly_contains(const Coord& P) const 
	{
		return (empty() ? false : (m_lbound.x<P.x && P.x<m_hbound.x && m_lbound.y<P.y && P.y<m_hbound.y));
	}


	double
	Bounds::width() const
	{
		return (empty() ? 0 : m_hbound.x-m_lbound.x);
	}

	
	double
	Bounds::height() const
	{
		return (empty() ? 0 : m_hbound.y-m_lbound.y);
	}


	Bounds&
	Bounds::operator+=(const Coord& coord)
	{
		if (!empty()) { 
			m_lbound+=coord;
			m_hbound+=coord;
		}
		return *this;
	}


	Bounds&
	Bounds::operator-=(const Coord& coord)
	{
		if (!empty()) { 
			m_lbound-=coord;
			m_hbound-=coord;
		}
		return *this;
	}


	void
	extend(Bounds& B, const Coord& P)
	{
		if (B.empty()) { B.m_lbound.x=B.m_hbound.x=P.x, B.m_lbound.y=B.m_hbound.y=P.y; B.m_empty=false; }
		else {
			B.m_lbound.x=min(B.m_lbound.x, P.x), B.m_lbound.y=min(B.m_lbound.y, P.y);
			B.m_hbound.x=max(B.m_hbound.x, P.x), B.m_hbound.y=max(B.m_hbound.y, P.y);
		}
	}

	
	void
	extend(Bounds& B, const Bounds& BX)
	{
		if (!BX.empty()) { 
			extend(B, BX.lbound()),  extend(B,BX.hbound());
		}
	}


	void
	intersect(Bounds& B, const Bounds& BI)
	{
		if (BI.empty() || B.empty() ) { B.clear(); }
		else {
			B.m_lbound.x=max(B.m_lbound.x, BI.m_lbound.x), B.m_lbound.y=max(B.m_lbound.y, BI.m_lbound.y);
			B.m_hbound.x=min(B.m_hbound.x, BI.m_hbound.x), B.m_hbound.y=min(B.m_hbound.y, BI.m_hbound.y);
			if (B.m_lbound.x>B.m_hbound.x || B.m_lbound.y>B.m_hbound.y) { B.clear(); }
		}
	}


	void
	grow(Bounds& B, const double& G)
	{
		if (!B.empty()) {
			B.m_lbound.x-=G, B.m_lbound.y-=G, B.m_hbound.x+=G, B.m_hbound.y+=G;
			if (B.m_lbound.x>B.m_hbound.x || B.m_lbound.y>B.m_hbound.y) { B.clear(); }
		}
	}


	Bounds
	operator+(const Bounds& bounds0, const Coord& coord)
	{
		Bounds	bounds1(bounds0);
		bounds1+=coord;
		return bounds1;
	}


	Bounds
	operator+(const Coord& coord, const Bounds& bounds0)
	{
		return bounds0+coord;
	}


	Bounds
	operator-(const Bounds& bounds0, const Coord& coord)
	{
		return bounds0+(-coord);
	}


} // namespace end
