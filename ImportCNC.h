#pragma once
#include "import.h"
#include <vector>


namespace fileIO
{
	class cnc_file : public file
	{

         enum currOperation {move = 0, line, arcCW, arcCCW};



public:
		cnc_file(std::map< int, element* >&, bool  mirrorX = false, bool mirrorY = false);
		~cnc_file();
        void Load(const QString&);
		virtual bool canLoad() { return true; }
		int	read_cnc(FILE*);
        std::vector<QString>	getTokens(QString);

private:

    void load_element(int, double, double, double, double, double, double, double, double, int);
	double calcLength(int elemType, Die::Coord _sp, Die::Coord _ep, Die::Coord _cp, double angle);

	bool							bMirrorX;
	bool							bMirrorY;
	_locale_t						L;
	
    currOperation                   m_lastOperation;
	
	};

}
