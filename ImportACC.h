#pragma once
#include "die_coord.h"
#include "Import.h"

class element;

namespace fileIO
{
    class acc_file:file
	{
		/*
		struct CLista
		{
			double subs[num_inst][5];
			int    subindex;
			char   subname[str_length];
		};
		*/

		enum op_acc {ACC_NONE = -1, ACC_C, ACC_SUB, ACC_END, ACC_Line, ACC_Arc,
			ACC_Tool, ACC_Macro, ACC_UI, ACC_CUSTOMER, ACC_DESIGNER, ACC_DELIVERYDATE,
			ACC_WOOD, ACC_MAX };
        enum acc_const{
			num_list=100, 
			str_length=255,
			//num_sector=11, 
			num_inst=900,
			max_bridge_num=500
		};
			
			bool			bMirrorX;
			bool			bMirrorY;
	
	public:
		acc_file( std::map< int, element* >& );
		virtual bool canLoad() { return true; }
        virtual void Load( const QString& fileName );

	private:
		int	read_acc(FILE*);
        void load_element(int,double,double,double,double,double,double,double,double,char*,double, int);
        double calcLength(int, Die::Coord, Die::Coord, Die::Coord, double);

        char *LeftTrim(char *pString)
        {
            size_t nLen = strlen(pString);
            size_t nPos = strspn(pString, " ");
            if ( nPos > nLen || nPos == 0 )
                return pString;
            memmove( pString, pString + nPos, nLen - nPos);
            pString[nLen-nPos] = 0;

            return pString;
        }
	private:
		int			cindex;
		char*		ctab[ACC_MAX];
		double		load_factor;
		double		arrow_height;
		double		x_format[4];
        std::map< int, QString >		acc_strings;
        int         m_currentTool;
	};

}
