#ifndef DIECADVIEWER_PLUGIN_H
#define DIECADVIEWER_PLUGIN_H

#include <QDeclarativeExtensionPlugin>

class DieCadViewerPlugin : public QDeclarativeExtensionPlugin
{
    Q_OBJECT
#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "DieCadViewer")
#endif

public:
    void registerTypes(const char *uri);
};

#endif // DIECADVIEWER_PLUGIN_H
