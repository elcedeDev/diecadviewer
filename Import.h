#pragma once
#ifndef IMPORT_H
#define IMPORT_H

class element;

namespace fileIO
{
	class file {
	public:
		std::map< int, element* >		&_elements;

        file( std::map< int, element* >& E ) : _elements( E ) {}
	};



}

#endif // IMPORT_H
