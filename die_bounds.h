#pragma once
#include "die_coord.h"


namespace Die
{
	
	class Bounds;



	// Bounds //////////////////////////////////////////////////////////////////

	class Bounds
	{
	public:
		inline Bounds();
		explicit inline Bounds(const Coord&, const Coord&);
		inline void clear();
		inline bool empty() const;
		inline bool	contains(const Coord&) const;
		inline bool	strictly_contains(const Coord&) const;
		inline double width() const;
		inline double height() const;
		inline Coord lbound() const;
		inline Coord hbound() const;
		inline Bounds extend(const Coord&) const;
		inline Bounds extend(const Bounds&) const;
		inline Bounds intersect(const Bounds&) const;
		inline Bounds grow(const double&) const;
		inline Bounds& operator+=(const Coord&);
		inline Bounds& operator-=(const Coord&);
		friend inline void extend(Bounds&, const Coord&);
		friend inline void extend(Bounds&, const Bounds&);
		friend inline void intersect(Bounds&, const Bounds&);
		friend inline void grow(Bounds&, const double&);
		friend inline Bounds operator+(const Coord&, const Bounds&);
		friend inline Bounds operator+(const Bounds&, const Coord&);
		friend inline Bounds operator-(const Bounds&, const Coord&);

	private:
		Coord			m_lbound;		
		Coord			m_hbound;
		bool			m_empty;
	};



} // namespace end


#include "die_bounds.hpp"
