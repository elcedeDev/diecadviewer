#pragma once


#include <QString>
#include <deque>


#define Die_pi2		1.57079632679489661922
#define Die_pi		3.14159265358979323844
#define Die_2pi		6.28318530717958647688
#define Die_3pi2	4.71238898038468985766
#define Die_radian	57.29577951308232087721
#define	Die_zero	0.000001

namespace Die 
{

	class Deg;
	class Rad;
	class Angle;
	class FreeAngle;
	class Coord;
	class CoordVec;
	class Base;

	// Deg /////////////////////////////////////////////////////////////////////

	class Deg
	{
		friend class Angle;
		friend class FreeAngle;
	public:
		inline Deg();
		inline explicit Deg(double);
		inline double radian() const;
	private:
		double			m_radian;
	};



	// Rad /////////////////////////////////////////////////////////////////////

	class Rad
	{
		friend class Angle;
		friend class FreeAngle;
	public:
		inline Rad();
		inline explicit Rad(double);
		inline double degree() const;
	private:
		double	m_radian;
	};



	// FreeAngle /////////////////////////////////////////////////////////////////

	class FreeAngle
	{
		friend class Angle;
	public:
		inline FreeAngle();
		inline FreeAngle(const Angle&);
		inline FreeAngle(const FreeAngle&);
		inline FreeAngle(const Deg&);
		inline FreeAngle(const Rad&);
		inline FreeAngle(const Coord&);
		inline double radian() const;
		inline double degree() const;

		inline FreeAngle& operator=(const FreeAngle&);
		inline FreeAngle& operator+=(const FreeAngle&);
		inline FreeAngle& operator-=(const FreeAngle&);
		inline FreeAngle& operator*=(const double&);
		inline FreeAngle& operator/=(const double&);

		friend inline FreeAngle operator+(const FreeAngle&,const FreeAngle&);
		friend inline FreeAngle operator-(const FreeAngle&);
		friend inline FreeAngle operator-(const FreeAngle&,const FreeAngle&);
		friend inline FreeAngle operator*(const FreeAngle&,const double&);
		friend inline FreeAngle operator*(const double&,const FreeAngle&);
		friend inline FreeAngle operator/(const FreeAngle&,const double&);

		inline void round( const int );

	private:
		double			m_radian;
	};



	// Angle ///////////////////////////////////////////////////////////////////

	class Angle
	{
		friend class FreeAngle;
	public:
		inline Angle();
		inline Angle(const Angle&);
		inline Angle(const FreeAngle&);
		inline Angle(const Deg&);
		inline Angle(const Rad&);
		inline Angle(const Coord&);

		inline double radian() const;
		inline double degree() const;

		inline Angle& operator+=(const FreeAngle&);
		inline Angle& operator-=(const FreeAngle&);
		inline Angle& operator*=(const double&);
		inline Angle& operator/=(const double&);
		inline Angle conjugate() const;
		friend inline Angle operator-(const Angle&);
		friend inline bool operator==(const Angle&, const Angle&);
		friend inline bool operator!=(const Angle&, const Angle&);
		friend inline Angle bisector(const Angle&, const Angle&);
	private:
		inline void normalize();
	
	private:
		double			m_radian;
	};



	// Coord ///////////////////////////////////////////////////////////////////

    class Coord
	{

	public:
		inline Coord();
		inline Coord(const Coord&);
		inline Coord(const double&, const double&);
		inline Coord(const double&, const FreeAngle&);
		inline explicit Coord(const FreeAngle&);

		inline double norm() const;
		inline double norm_square() const;
		inline Coord normalize() const;
		inline Coord normalize(const double&) const;
		inline Coord rnormal() const;
		inline Coord lnormal() const;
		inline Coord rotate(const Coord&) const;
		inline Coord conjugate() const;
		//inline Coord scale(const Coord&) const;

        inline Coord& operator=( QString& );
        //inline Coord& operator=( TCHAR* );
		inline Coord& operator=(const Coord&);
		inline Coord& operator+=(const Coord&); 
		inline Coord& operator-=(const Coord&);
		//inline Coord& operator*=(const Matrix&);					// deprecated
		inline Coord& operator*=(const double&);
		inline Coord& operator/=(const double&);

		friend inline void rotate(Coord&, const Coord&);
		friend inline bool operator==(const Coord&, const Coord&);
		friend inline bool operator!=(const Coord&, const Coord&);
		friend inline Coord operator+(const Coord&, const Coord&);
		friend inline Coord operator-(const Coord&, const Coord&);
		friend inline Coord operator-(const Coord&);
		friend inline Coord operator*(const Coord&, const double&);
		friend inline Coord operator*(const double&, const Coord&);
		friend inline Coord operator/(const Coord&, const double&);
		friend inline bool parallel(const Coord&, const Coord&);
		//friend inline bool crossing(Coord, Coord, Coord);			// deprecated
		friend inline double lambda(Coord, Coord, Coord);
		friend inline double cross(const Coord&, const Coord&);

		inline bool operator < (const Coord & rhs) const { return (x < rhs.x) ? true : (x == rhs.x) && (y < rhs.y); } 
		inline Coord& round( const int N ) {
			x *= pow( 10.0, N );						y *= pow( 10.0, N );
			x = int( x + (x < 0 ? -0.5 : 0.5 ) );		y = int( y + (y < 0 ? -0.5 : 0.5 ) );
			x /= pow( 10.0, N );						y /= pow( 10.0, N );
			return *this;
		}
	public:
		double			x, y;


	};



	// CoordVec ////////////////////////////////////////////////////////////////

	class CoordVec 
		: public std::deque<Coord>
	{
	public:
		typedef	iterator			Iterator;
		typedef const_iterator		ConstIterator;
	};



	// Base ////////////////////////////////////////////////////////////////////
	
	class Base
	{
	public:
		inline Base();
		inline Base(const Die::Coord&, const Die::Coord&);
		inline Base invert() const;
		inline Die::Coord transform(const Die::Coord&) const;
		inline Base transform(const Base&) const;

	public:
		Die::Coord		x;
		Die::Coord		y;
	};



} // namespace end



#include <deque>



#include "die_coord.hpp"
