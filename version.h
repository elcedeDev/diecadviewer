#ifndef VERSION_H
#define VERSION_H


#define VER_FILEVERSION             3,5,0,0
#define VER_FILEVERSION_STR         "3.5.0.0"

#define VER_PRODUCTVERSION          VER_FILEVERSION
#define VER_PRODUCTVERSION_STR      VER_FILEVERSION_STR

#define VER_COMPANYNAME_STR         "ELCEDE GmbH"
#define VER_FILEDESCRIPTION_STR     "Die CAD visualizer for VV8 - 1.3.8"
#define VER_INTERNALNAME_STR        "DieCadViewer plugin"
#define VER_LEGALCOPYRIGHT_STR      "Copyright \xa9 2018 ELCEDE GmbH"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "DieCadViewer.dll"
#define VER_PRODUCTNAME_STR         VER_INTERNALNAME_STR

#define VER_COMPANYDOMAIN_STR       "https://www.elcede.de/"




#define PRINTVERSION QString strDate = __DATE__;\
    QStringList lstDate = strDate.split(QRegExp("\\s+"), QString::SkipEmptyParts);\
    QLocale us = QLocale("en_US");\
    int year = us.toDate(lstDate[2], "yyyy").year();\
    int month = us.toDate(lstDate[0], "MMM").month();\
    int day = us.toDate(lstDate[1], "d").day();\
    QDate date = QDate(year, month, day);\
    QString buildDate =  date.toString(Qt::ISODate); \
    qDebug() << VER_PRODUCTNAME_STR << "|" << "Version"  <<  VER_FILEVERSION_STR << "|" <<  "Build date" << buildDate.toLatin1().data() << __TIME__;


//#define PRINTVERSION    qDebug() << VER_PRODUCTNAME_STR << "|" << "Version"  <<  VER_FILEVERSION_STR << "|" <<  "Build date" << __DATE__ << __TIME__;

#endif // VERSION_H

