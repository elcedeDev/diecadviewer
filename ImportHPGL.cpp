#include "mathfunctions.h"
#include "die_coord.h"
#include "element.h"
#include "ImportHPGL.h"
#include <locale.h>
#include <QDebug>

namespace fileIO {

	hpgl_file::hpgl_file( std::map< int, element* >& E, bool isLoad ) : file( E ), m_isLoading(isLoad)
	{
		m_bMirrorX = false;
		m_bMirrorY = false;
		m_pen.status = PEN_UP;
		m_pen.move = PEN_ABSOLUTE;
        m_pen.number = 0;
        m_pen.x = 0;
		m_pen.y = 0;
		L = _create_locale(LC_ALL, "en-US");
	}

	hpgl_file::~hpgl_file()
	{
		_free_locale(L);
		if(m_isLoading)
			free(m_data);
	}

/*********************************************
* function void hpgl_file::Load( const CString& fileName )
*
* opens the HPGL input file
* calls the read_hpgl function
* tokenizes content of data buffer and 
* calls the process_command for every tokens
* const CString& fileName: the name of te input file 
**********************************************/
    void hpgl_file::Load( const QString& fileName)
	{
		FILE			*fp			= NULL;
		char			*token		= NULL;
		char			*context	= NULL;
		char			delims[]	= " ;,\t\r\n";
		int				hpgl_cmd; 
		size_t			tokenPos;
		std::string		s;

		m_E	= NULL;						
        if (_wfopen_s(&fp, fileName.toStdWString().c_str(), L"rb")==0) {
			if(read_hpgl(fp) == 0) {
				token = strtok_s(m_data, delims, &context);
				while (token != NULL) {
                    s = token;
					//lets take the first 2 chars
					hpgl_cmd = MAKE_HPGL_CMD( toupper( token[0] ), toupper( token[1] ) );
					// while we have valid hpgl command		
					while( hpgl_cmd >= HPGL_AA ) {

						process_command( hpgl_cmd );
						//process the glued commands e.g INPU
						if( s.size() >= 3 ) {
							for( tokenPos = 2; tokenPos <= (s.size() - 1); ){
								hpgl_cmd = MAKE_HPGL_CMD( toupper( token[tokenPos] ), toupper( token[tokenPos+1] ) );
								if( hpgl_cmd >= HPGL_AA ){
									process_command( hpgl_cmd );
									tokenPos += 2;
								}else
									break;
							}//for
							hpgl_cmd = 0;
							if ((int(s.size() - tokenPos)) >= 0){
								s.assign(token, tokenPos, s.size() - tokenPos);
							}
							else{
								s = "";
							}
						}else
							hpgl_cmd = 0;
					}//while
					if( s.size() > 0 )
						process_param( s );
					token = strtok_s(NULL, delims, &context);
				} //while
			}//if
		}//if
		//file.Close();
	
	}//Load

	/****************************************************
	* function int hpgl_file::process_param(std::string param)
	* received string must contains numbers
	*
	*****************************************************/
	int hpgl_file::process_param(std::string &param)
	{
        //qDebug() << "hpgl_file::process_param " << param.c_str();
		//double px, py;
//		char c = param[0];
		if( (int)param[0] <= 57 ){
			
			double d_param =  _strtod_l( param.c_str(),'\0', L );
		
			switch(m_last_cmd)
			{
			case HPGL_AA:								//ARC ABSOLUTE
				//note: the qd -- the optional 4. param is not handled here
				if( m_param_counter == 2 ){
					m_angle = d_param;
					//this is the angle param in degree -- convert to radian
                    double p_angle =  d_param * Die_pi/180;
					// calculate the end coordinates from the angle
					double length;				// distance between start and center
					double angle;				// direction agle of start - center line

					m_start.x = m_pen.x;
					m_start.y = m_pen.y;
					length = sqrt( pow((m_start.x - m_center.x),2.0) + pow((m_start.y - m_center.y),2.0) );
					if( length != 0 )			// to avoid devide by zero
						angle = acos( ( m_start.x - m_center.x ) / length );
					else
						angle = 0;

					if( m_start.y < m_center.y )
                        angle = Die_2pi - angle;
				
					m_end.x = m_center.x + length * cos( p_angle  + angle ); 
					m_end.y = m_center.y + length * sin( p_angle  + angle ); 

					//hpgl graphics draw clockwise if the angle is negative
					//swith the start - end if the angle is negative
					Die::Coord tmp = m_end;
                    m_pen.x = m_end.x;
                    m_pen.y = m_end.y;
					load_element(element::ET_ARC);
					//switch back the start position
					m_start = tmp;

				}else{
					if( m_param_counter == 0 ) //center x
						m_center.x = d_param;
					else
						m_center.y = d_param;
				}
				break;

			case HPGL_SP:								// SELECT PEN
				m_pen.number = (int)d_param;
                break;

            case HPGL_CI:								// CIRCLE
				if (m_param_counter == 0) {
					m_center = m_start;
					m_start = Die::Coord(m_center.x - d_param, m_center.y);
					m_end = m_start;
					m_angle = 360.0;
                    load_element(element::ET_ARC);
				}
				break;
			case HPGL_PA:								// PLOT ABSOLUTE
				setPenCoords(d_param);
                if( m_param_counter == 1 )
                {
                    load_element(element::ET_LINE);
                }
				break;

			case HPGL_PU:								// PEN UP
                //m_pen.status = PEN_UP;
				setPenCoords(d_param);
                if( m_param_counter == 1 ){
					// save the pen position in start coord
					m_start.x = m_pen.x;
					m_start.y = m_pen.y;
            }
				break;
			case HPGL_PD:								// PEN DOWN
                //m_pen.status = PEN_DOWN;
				setPenCoords(d_param);
				if( (m_param_counter % 2) == 1)
				{
                    //qDebug() << " case HPGL_PD:	  (m_param_counter % 2) == 1 :m_param_counter" << m_param_counter << " param: " << param.c_str() << " m_pen.number: " <<  m_pen.number;
                    load_element(element::ET_LINE);
					//if we have more line
					if( m_param_counter >= 3 )
					{   // set the end coords of line and save the line
                        //qDebug() << " case HPGL_PD:	  m_param_counter >= 3 :m_param_counter" << m_param_counter << " param: " << param.c_str() << " m_pen.number: " <<  m_pen.number;
                        load_element(element::ET_LINE);
					}else{
						m_start.x = m_pen.x;
						m_start.y = m_pen.y;
					}
				}

				break;
		
			default:
				break;
			}
		
			++m_param_counter;
		}
		return 0;

	}

	/****************************************************
	* function int hpgl_file::setPenCoords(double dcoord)
	* sets the pen x or y coords
	* it takes into account the pen.move options
	*****************************************************/
	void hpgl_file::setPenCoords(double dcoord)
	{
		// m_param_counter = 0 -> x coord, 1 -> y
			if( m_param_counter % 2 == 0 ){			// x coord
				if (m_pen.move == PEN_RELATIVE)		// Relative draw 
					m_pen.x += dcoord;
				else
					m_pen.x = dcoord;
			}else{									// y coord
				if (m_pen.move == PEN_RELATIVE)		// Relative draw 
					m_pen.y += dcoord;
				else
					m_pen.y = dcoord;
			}

	}

	/****************************************************
	* function void hpgl_file::load_element()
	* saves a line element into the _elements list
	*****************************************************/
    void hpgl_file::load_element(int etype)
	{
		
		m_E = new element();
		if(etype == element::ET_LINE ){
			m_E->setType( element::ET_LINE);
			m_end.x = m_pen.x;
			m_end.y = m_pen.y;
			m_E->setLength(calcLength(element::ET_LINE, Die::Coord(m_start.x, m_start.y), Die::Coord(m_end.x, m_end.y)  , Die::Coord(0,0), 0));
		}else{
			m_E->setType( element::ET_ARC );
			m_E->setCoord( element::CID_CENTER ,Die::Coord(m_center.x, m_center.y));
			m_E->setDouble(element::DID_ARCANGLE, m_angle);
			m_E->setLength(calcLength(element::ET_ARC, Die::Coord(m_start.x , m_start.y)  , Die::Coord(m_end.x, m_end.y)  , Die::Coord(m_center.x, m_center.y), m_angle));
		}
		m_E->setCoord( element::CID_START, Die::Coord(m_start.x, m_start.y)  );
		m_E->setCoord( element::CID_END, Die::Coord(m_end.x, m_end.y));
		m_E->setInt( element::IID_LINETYPE, 12 );
		m_E->setDouble( element::DID_AUXTYPE, 12.0 );		
		m_E->setDouble( element::DID_POINTAGE, 2.0 );	
		m_E->setDouble(element::DID_BRIDGELENGTH,0);
		m_E->setInt(element::IID_BRIDGENUM, 0);
        m_E->setInt(element::IID_TOOL, m_pen.status == PEN_DOWN ? m_pen.number : 0);
        m_E->setInt(element::IID_LINENUM, (int) _elements.size());
        m_E->setInt(element::IID_ID, (int) _elements.size());

        _elements[ _elements.size() ] = m_E;
		m_E = NULL;
		// save the end position as the start for a new line if there is any
		//if( etype == element::ET_LINE )
        m_start = m_end;

	}
		
	double hpgl_file::calcLength(int elemType, Die::Coord _sp, Die::Coord _ep, Die::Coord _cp, double angle)
	{

		double length = 0.0;

		//		double x1, x2, y1, y2, m1, m2, degree;
		double radius;

		if (elemType == element::ET_LINE) {
			length += sqrt(((_sp.x - _ep.x)*(_sp.x - _ep.x)) + ((_sp.y - _ep.y)*(_sp.y - _ep.y)));

		}
		else if (elemType == element::ET_ARC) {

			radius = (_sp - _cp).norm();

            length += radius * angle * (Die_pi / 180);
		}


		return length;


	}
/*********************************************
* function int hpgl_file::process_command( char *command )
*
* processes commands filtered from the input HPGL file
* struct pen *p: structure contains pen information
* char *command: the command received from the input HPGL file
**********************************************/
	int hpgl_file::process_command( int command )
	{
		
		switch(command)
		{
		case HPGL_IN:
			//initializer command
			break;
		case HPGL_AA:		//ARC ABSOLUTE
			m_pen.status = PEN_DOWN;
			break;
		case HPGL_PU:
			m_pen.status = PEN_UP;
			break;
		case HPGL_PD:
			m_pen.status = PEN_DOWN;
			break;
		case HPGL_SP:		//SELECT PEN
			break;
		case HPGL_CI:		//CIRCLE
			break;
		case HPGL_DI:		//ABSOLUTE DIRECTION
			break;
		case HPGL_PA:		//Plot Absolute
			m_pen.move = PEN_ABSOLUTE;
			break;
		case HPGL_PR:		//Relative Coordinate Pen Move
			m_pen.move = PEN_RELATIVE;
			break;
		default:
			//fprintf(stderr,"ERROR: Don't know how to handle command '%s'\n", command);
			break;
		}
		//save the coammnd
		m_last_cmd = command;
		//reset the parameter counter
		m_param_counter = 0;

		return 0;
	}
//eof -- hpgl_file::process_command


/*********************************************
* function int hpgl_file::read_hpgl(FILE *fp)
*
* obtains the size of the HPGL input file
* allocates memory for the whole content
* loads the content into the data member 
* closes the file
* FILE *fp: the input HPGL file
**********************************************/
	int hpgl_file::read_hpgl(FILE *fp)
	{
		long lSize;
		size_t result;
        // obtain file size:
		fseek (fp , 0 , SEEK_END);
		lSize = ftell (fp);
		rewind (fp);
        // allocate memory to contain the whole file:
		m_data = (char*) malloc (sizeof(char)*lSize + 1000);
		if (m_data == NULL) {fputs ("Memory error",stderr); exit (2);}
		// copy the file into the data buffer:
		result = fread (m_data, sizeof(char), lSize, fp);
		if (result != lSize) {fputs ("Reading error",stderr); exit (3);}
		m_data[lSize] = '\0';			
		fclose(fp);
		return 0;
	}
}
