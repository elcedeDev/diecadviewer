#ifndef TEXTFIELDDOUBLEVALIDATOR_H
#define TEXTFIELDDOUBLEVALIDATOR_H

#include <QDoubleValidator>

class TextFieldDoubleValidator : public QDoubleValidator
{
public:
    TextFieldDoubleValidator (QObject * parent = 0) : QDoubleValidator(parent) {}
    TextFieldDoubleValidator (double bottom, double top, int decimals, QObject * parent) :
        QDoubleValidator(bottom, top, decimals, parent) {}

    QValidator::State validate(QString & s, int & pos) const {
        Q_UNUSED(pos);

        if (s.isEmpty() || (s.startsWith("-") && s.length() == 1)) {
            // allow empty field or standalone minus sign
            //qDebug() << "1 TextFieldDoubleValidator -- if (s.isEmpty() || (s.startsWith(" << "-) && s.length() == 1))" << s;
            return QValidator::Intermediate;
        }
        if (s.isEmpty() || (s.startsWith("-.") && s.length() == 2)) {
            // allow empty field or minus sign with .
            //qDebug() << "11 TextFieldDoubleValidator -- if (s.isEmpty() || (s.startsWith(" << "-) && s.length() == 1))" << s;
            return QValidator::Intermediate;
        }
        if (s.isEmpty() || (s.startsWith("-,") && s.length() == 2)) {
            // allow empty field or minus sign with ,
            //qDebug() << "12 TextFieldDoubleValidator -- if (s.isEmpty() || (s.startsWith(" << "-) && s.length() == 1))" << s;
            return QValidator::Intermediate;
        }
        if (s.isEmpty() || (s.startsWith(".") && s.length() == 1)) {
            // allow empty field or standalone "."
            //qDebug() << "2 TextFieldDoubleValidator -- if (s.isEmpty() || (s.startsWith(" << ".) && s.length() == 1))" << s;
            return QValidator::Intermediate;
        }
        if (s.isEmpty() || (s.startsWith(",") && s.length() == 1)) {
            // allow empty field or standalone ","
            //qDebug() << "3 TextFieldDoubleValidator -- if (s.isEmpty() || (s.startsWith(" << ".) && s.length() == 1))" << s;
            return QValidator::Intermediate;
        }


        QChar point = locale().decimalPoint();
        QString s2;
        if(point == "."){
            s2 = s;
            s2.replace(",", ".");
        }
        if(point == ","){
            s2 = s;
            s2.replace(".", ",");
        }
        // check length of decimal places
        if(s2.indexOf(point) != -1) {
            int lengthDecimals = s2.length() - s2.indexOf(point) - 1;
            if (lengthDecimals > decimals()) {
                //qDebug() << "4 TextFieldDoubleValidator -- if (lengthDecimals > decimals())" << s;
                return QValidator::Invalid;
            }
        }
        // check range of value
        bool isNumber;
        double value = locale().toDouble(s2, &isNumber);
        if (isNumber && bottom() <= value && value <= top()) {
            //qDebug() << "5 TextFieldDoubleValidator -- if (isNumber && bottom() <= value && value <= top())" << s;
            return QValidator::Acceptable;
        }
        //qDebug() << "6 TextFieldDoubleValidator -- QValidator::Invalid" << s;
        return QValidator::Invalid;

    }


};



#endif // TEXTFIELDDOUBLEVALIDATOR_H
