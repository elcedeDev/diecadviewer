
#include "element.h"


element::element(void)
{
	_isHighlighted = false;
	_materialID = -1;
	_color = -1;
}


element::~element(void)
{

}


void element::setLength(double L)
{
	_length = L;
}

void element::setType( int T )
{
	_type = T;
}

void element::setCoord( int I, const Die::Coord& C)
{
	_coords[ I ] = C;
}

void element::setInt( int I, int V )
{
	_ints[ I ] = V;
}

void element::setDouble( int I, double D )
{
	_doubles[ I ] = D;
}

void element::setText( int I, QString S)
{
	_strings[ I ] = S;
}
			
double	element::getLength()
{
	return _length;
}

int		element::getType()
{
	return _type;
}

int		element::getInt( int V )
{
	intsType::iterator	I = _ints.find( V );
    if( I == _ints.end() ) return -1;
	return I->second;
}

double	element::getDouble( int V )
{
	doublesType::iterator	I = _doubles.find( V );
    if( I == _doubles.end() ) return -1;
	return I->second;
}

Die::Coord	element::getCoord( int V )
{
	coordsType::iterator	I = _coords.find( V );
    if( I == _coords.end() ) return Die::Coord(-9999,-9999);
	return I->second;
}

QString	element::getString( int V )
{
	StringsType::iterator	I = _strings.find( V );
    if( I == _strings.end() ) return QString();
	return I->second;
}
double pDistance(double x, double y, double x1, double y1, double x2, double y2) {

	double A = x - x1;
	double B = y - y1;
	double C = x2 - x1;
	double D = y2 - y1;

	double dot = A * C + B * D;
	double len_sq = C * C + D * D;
	double param = dot / len_sq;

	double xx, yy;

	if (param < 0 || (x1 == x2 && y1 == y2)) {
		xx = x1;
		yy = y1;
	}
	else if (param > 1) {
		xx = x2;
		yy = y2;
	}
	else {
		xx = x1 + param * C;
		yy = y1 + param * D;
	}

	double dx = x - xx;
	double dy = y - yy;
	return sqrt(dx * dx + dy * dy);
}
double element::getDistance( const Die::Coord& hitCoord )
{

	if( _type == ET_LINE ) {
		Die::Coord sp = _coords[CID_START];
		Die::Coord ep = _coords[CID_END];
		Die::Coord u = sp - ep;
		Die::Coord v=u.lnormal();
		Die::Coord q=ep - hitCoord;
		Die::Coord d;
		double lambda=Die::lambda(v, q, u);
		
		if		(lambda<=0) { d=ep - hitCoord; }
		else if (lambda>=1) { d= sp - hitCoord; }
		else				{ d=q+u*lambda; }

        double ret2 = pDistance(hitCoord.x, hitCoord.y, sp.x, sp.y, ep.x, ep.y);
		return ret2;/*
		double d = abs(((ep.x - sp.x) * (sp.y - hitCoord.y)) - ((sp.x - hitCoord.x) * (ep.y - sp.y))) /
			sqrt(((ep.x - sp.x) * (ep.x - sp.x)) + ((ep.y - sp.y) * (ep.y - sp.y)));
		return d;*/
	}
	else if (_type == ET_ARC){
		Die::Coord sp = _coords[CID_START];
		Die::Coord ep = _coords[CID_END];
		Die::Coord cp = _coords[CID_CENTER];
		double PtA = Die::Angle(hitCoord - cp).radian();
        double StA;
        double EnA;
        if(getDouble(DID_ARCANGLE)> 0){
            StA = Die::Angle(sp - cp).radian();
            EnA = Die::Angle(ep - cp).radian();
        }else{
            StA = Die::Angle(ep - cp).radian();
            EnA = Die::Angle(sp - cp).radian();

        }
		if ((StA < PtA && PtA < EnA) || ((StA > EnA) && (PtA > StA || PtA < EnA))
			){
			double r = (sp - cp).norm();
			double cd = (hitCoord - cp).norm();
			return fabs(cd - r);
		}
		else if (math::closeto((sp - ep).norm(), 0, math::_generalTolerance)) {
			double r = (sp - cp).norm();
			double cd = (hitCoord - cp).norm();
			return fabs(cd - r);
		}
		double SL = (sp - hitCoord).norm_square();
		double EL = (ep - hitCoord).norm_square();
		return SL < EL ? sqrt(SL) : sqrt(EL);

	}
	return 100;
}

Die::Coord element::lambda( double dist )
{
	Die::Coord		sp = _coords[CID_START];
	Die::Coord		ep = _coords[CID_END];

	return ( sp + (ep-sp)*dist );
}

Die::Angle element::getAngle( )
{
	return Die::Angle( _coords[CID_END] - _coords[CID_START] );
}

Die::Bounds	element::getBoundingBox()
{
	Die::Bounds			boundingBox;
	Die::Coord _sp = getCoord(CID_START);
	Die::Coord _ep = getCoord(CID_END);

	if (_type == ET_ARC){
	

		Die::Coord _cp = getCoord(CID_CENTER);
		double r = sqrt(((_sp.x - _cp.x)*(_sp.x - _cp.x)) + (_sp.y - _cp.y)*(_sp.y - _cp.y));

		if (math::closeto((_sp - _ep).norm(), 0, /*math::_generalTolerance*/ 0.01)) {
			Die::extend(boundingBox, _cp + Die::Coord(r, r));
			Die::extend(boundingBox, _cp - Die::Coord(r, r));
		}
		else {
			Die::Coord		Q, S(_ep - _sp);
			Die::extend(boundingBox, _sp);
			Die::extend(boundingBox, _ep);
			for (int i = 0; i<4; ++i) {
				Q = _cp + Die::Coord((i / 2 ? (i % 2 ? r : -r) : 0),
					(i / 2 ? 0 : (i % 2 ? r : -r)));
                double angle = getDouble(DID_ARCANGLE);
				if (angle > 0){
					if (Die::cross(S, Q - _sp) < 0) {
						Die::extend(boundingBox, Q);
					}
				}
				else{
					if (Die::cross(S, Q - _sp) > 0) {
						Die::extend(boundingBox, Q);
					}
				}
			}
		}
	}
	else if (_type == ET_LINE){

		Die::extend(boundingBox, _sp);
		Die::extend(boundingBox, _ep);

	}
	return boundingBox;
}
