import QtQuick 1.1
import Elcede 1.0


FocusScope {
    id: mainrect
    objectName: "aradexRoot"
    smooth: S.isFast
    width: 1280
    height: 1024

    property real   originalWidth
    property real   originalHeight
    property real   thisScale
    property string rootStyle: S.styleName
    property string imagePath: S.Style[mainrect.rootStyle].imagePath

    //-- props used in job screen
    property bool   bshowdirection: false
    property bool   bshowtool0: false


	//	signal setResizeMode
    //  iMode=0: SizeRootObjectToView (standard behave, frame windows does not change size)
    //  iMode=1: SizeViewToRootObject ( framewindow changes size according to the size of the root object (mainrect)
    signal setResizeMode(int iMode)

    // move the main window
    signal move(int x, int y)
	
	// signal that is received from the viewer on moving for the virtual keyboard
	signal movedForKeyboard(int x, int y)
	
	// signal that is received in c++ code
	signal styleBackgroundColor(color colBackground)

    // signal that is received in c++ code on style change
    signal styleChanged(string newStyle)
    onStyleChanged: {
        rootStyle = newStyle
        styleBackgroundColor(S.Style[mainrect.rootStyle].backgroundColor)
    }
	


    // signal that is received in c++ code
    signal scaledSizeChanged(real minScale)
    onScaledSizeChanged: thisScale = minScale
    signal languageChanged()
    signal timerActivated()

    // this signal needs to be emitted if the style is to be changed from root object
    signal changeRootStyle(string newStyle)

    onRootStyleChanged:{
        styleChanged(rootStyle)
        var stateTemp = mainrect.state
        mainrect.state = ""
        mainrect.state = stateTemp
    }

    function clickRoot() {
        swipearea.forceActiveFocus()
    }

    //--changeJobScreen by the ToolbarButton to switch the job screens
    function changeJobScreen(){
        if( screenJob.state === "1" )
            screenJob.state = "0"
        else
            screenJob.state = "1"
    }


    function changeState(){

        switch(flickable.currentIndex){
        case 0: mainrect.state = "Setup"
            break;
        case 1: mainrect.state = "Material"
            break;
        case 2: mainrect.state = "Magazine"
            break;
        case 3: mainrect.state = "Job"
            break;
        case 4: mainrect.state = "Statistics"
            break;
        case 5: mainrect.state = "Service"
            break;
        }

    }

    Component.onCompleted: {
        originalWidth = width
        originalHeight = height
    }
	
	// signal that is emitted by the viewer app when the application is ready to run
    signal applicationStart()
    onApplicationStart: {
	}

    DialogFileChooser {
        id: material_file_load
        x: 851
        y: 39
        opacity: 1
        fileChooserText: "Load File"
        colorChoice: 1
        localFileSystem: false
        currentPath: "/opt/v8/"
        buttonCancelText: qsTr("Cancel")
        buttonOkText: qsTr("Load")
        titleText: qsTr("Load Material")
        onOk: {
           Material_strFile.value = currentPath + currentFileName;
           Material_bLoad.value = true;
        }
    }


    Rectangle{
        id: backgroundRectangle
        anchors.fill: parent
        color: S.Style[mainrect.rootStyle].backgroundColor
        x: -1

        DialogFileChooser {
            id: openfile_material
            width: 300
            height: 600
            titleText: qsTr("Load Material")
            currentPath: "/opt/v8/application/machine/"
            localFileSystem: false
            buttonOkText: qsTr("Load")
            visible: false
            opacity: 1
            onOk: text_material_file_input.text = currentPath + currentFileName
        }

        Timer {
                id: timer
            }

        function delay(delayTime, cb) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.start();
        }

        DialogFileChooser {
            id: openfile_job
            width: 300
            height: 600
            titleText: qsTr("Load Job")
            currentPath: "/opt/v8/application/machine/"
            localFileSystem: false
            buttonOkText: "Load"
            visible: false
            opacity: 1
            onOk: {
                Job_strFile.value = currentPath + currentFileName;
                Job_InOut_bLoadJob.value = true;
                dieCadViewer_small.filename = "files:z:\\application\\machine\\LoadedJob.acc"
                dieCadViewer_large.filename = "files:z:\\application\\machine\\LoadedJob.acc"
                VV_bLoadJobTrigger.value = true
            }
        }

        Image{
            id: backgroundImage
            anchors.fill: parent
            source: imagePath + "Hintergrund.png"
        }

        Image {
            id: en_main
            x: 261
            y: 122
            width: 987
            height: 651
            clip: false
            sourceSize.width: 987
            sourceSize.height: 651
            source: imagePath + "EN_Main.png"
        }

        //--ELCEDE screen header{
                Image{
                    id: elcedelogo
                    anchors.left: parent.left
                    anchors.leftMargin: 30
                    anchors.top: parent.top
                    anchors.topMargin: 30
                    source: imagePath + "ELCEDELogo.svg"
                }
                Rectangle{
                    id: elcedeLine
                    anchors.top: parent.top
                    anchors.topMargin: 76
                    anchors.left: elcedelogo.right
                    anchors.leftMargin: 10
                    height: 10
                    anchors.right: parent.right
                    anchors.rightMargin: 30
                    color: "#991b1e"
                }
                Image{
                    id: machinelogo
                    anchors.right: parent.right
                    anchors.rightMargin: 38
                    anchors.top: parent.top
                    anchors.topMargin: 30
                    source: "images/CounterMATEevo.svg"
                }
                GroupBox{
                    id: label_current_date
                    x: 1000
                    y: 80
                    Image{
                        source:"images/Date.bmp"
                    }
                    Label{
                        fontSize:12
                        textIO:"LocalTime_Out_strDate"
                        //text:"date"

                    }
                }

                GroupBox{
                    id: label_current_time
                    x: 1120
                    y: 80
                    Image{
                        source:"images/Time.bmp"
                    }
                  Label{
                      fontSize:12
                      textIO:"LocalTime_Out_strTime"
                      //text:"time"
                  }
                }
                //--ELCEDE screen header}


                Image {
                    id: toolbarImage
                    x: 30
                    y: 783
                    width: 1221
                    height: 211
                    clip: false
                    //sourceSize.width: 987
                    //sourceSize.height: 651
                    source: imagePath + "toolbar.png"
                }

        SwipeArea{
            id: swipearea
            x: 0
            y: 0

            ready: false
      //      anchors.top: elcedelogo.bottom
      //      anchors.fill: parent
            onReleaseNoDrag: forceActiveFocus()
            onMove: mainrect.move(x,y)

            GroupBox {
                id: mainContainer
                x: 263
                y: 124
                width: 991
                height: 655

                colorChoice: 1
                headerVisible: false

                Item {
                    id: pagesContainer
                    clip: true
                    width: parent.width -8
                    height: parent.height -8
                    VisualItemModel{
                        id: pages

                        ScreenSetup{id:screenSetup}
                        ScreenMaterial{}
                        ScreenMagazine{}
                        ScreenJob{id:screenJob}
                        ScreenStatistics{}
                        ScreenService{}
                    }


                    ListView {
                        id: flickable
                        anchors.fill: parent
                        focus: true
                        highlightRangeMode: ListView.StrictlyEnforceRange
                        orientation: ListView.Horizontal
                        snapMode: ListView.SnapOneItem
                        flickDeceleration: 500
                        highlightMoveDuration:500
                        cacheBuffer: 7812
                        model: pages
                        onMovementEnded:changeState()
  
                        delegate: Item { }
                    }

                }

            }

            RadioGroup {
                id: chooseScreensGroup
                x: 30
                y: 145
                width: 136
                height: 356
                initialState: "Setup"
                stateTarget: "mainrect"

                Keys.onPressed: {
                        if (event.key === Qt.Key_Escape) {
                            //console.log("RadioGroup Key_Escape");
                            event.accepted = true;
                         }
                    }
                MenuButton{
                    id:radio_Setup_Menu

                    __style: mainrect.rootStyle
                    __text: qsTr("Setup")
                    __normalIcon: "MSetupUp"
                    __pressedIcon: "MSetupDown"
                    __imagePath: imagePath
                    onClicked_changeStateTo: "Setup"
                    onClicked: flickable.currentIndex = 0
                     __pressed: flickable.currentIndex === 0
                }

                MenuButton{
                    id:radio_Material_Menu

                    anchors.top: radio_Setup_Menu.bottom
                    anchors.topMargin: 15

                    __style: mainrect.rootStyle
                    __text: qsTr("Material")
                    __normalIcon: "MMaterialUp"
                    __pressedIcon: "MMaterialDown"
                    __imagePath: imagePath
                    onClicked_changeStateTo: "Material"
                    onClicked: flickable.currentIndex = 1
                    __pressed: flickable.currentIndex === 1
                }

                MenuButton{
                    id:radio_Magazine_Menu

                    anchors.top: radio_Material_Menu.bottom
                    anchors.topMargin: 15

                    __style: mainrect.rootStyle
                    __text: qsTr("Magazine")
                    __normalIcon: "MMagazineUp"
                    __pressedIcon: "MMagazineDown"
                    __imagePath: imagePath
                    onClicked_changeStateTo: "Magazine"
                    onClicked: flickable.currentIndex = 2
                    __pressed: flickable.currentIndex === 2
                }


                MenuButton{
                    id:radio_Job_Menu

                    anchors.top: radio_Magazine_Menu.bottom
                    anchors.topMargin: 15

                    __style: mainrect.rootStyle
                    __text: qsTr("Job")
                    __normalIcon: "MJobUp"
                    __pressedIcon: "MJobDown"
                    __imagePath: imagePath
                    onClicked_changeStateTo: "Job"
                    onClicked: flickable.currentIndex = 3
                    __pressed: flickable.currentIndex === 3
                }

                MenuButton{
                    id:radio_Statistics_Menu

                    anchors.top: radio_Job_Menu.bottom
                    anchors.topMargin: 15

                    __style: mainrect.rootStyle
                    __text: qsTr("Statistics")
                    __normalIcon: "MStatisticsUp"
                    __pressedIcon: "MStatisticsDown"
                    __imagePath: imagePath
                    onClicked_changeStateTo: "Statistics"
                    onClicked: flickable.currentIndex = 4
                    __pressed: flickable.currentIndex === 4
                }

                MenuButton{
                    id:radio_Service_Menu

                    anchors.top: radio_Statistics_Menu.bottom
                    anchors.topMargin: 15

                    __style: mainrect.rootStyle
                    __text: qsTr("Service")
                    __normalIcon: "MServiceUp"
                    __pressedIcon: "MServiceDown"
                    __imagePath: imagePath
                    onClicked_changeStateTo: "Service"
                    onClicked: flickable.currentIndex = 5
                    __pressed: flickable.currentIndex === 5
                }

                MenuButton{
                    id:radio_Exit_Menu

                    anchors.top: radio_Statistics_Menu.bottom
                    anchors.topMargin: 295


                    __style: mainrect.rootStyle
                    __text: qsTr("Exit")
                    __normalIcon: "MExitUp"
                    __pressedIcon: "MExitDown"
                    __imagePath: imagePath

                    onClicked: { Qt.quit(); }


                }

// @RadioButton@
            }
        }
        //loads the toolbar according to the currebt state
        Loader{ id:toolBarLoader }


    }



    states: [
        State {
            name: "Setup"
            PropertyChanges {
                target: toolBarLoader
                source: "ToolbarSetup.qml"
            }
        },
        State {
            name: "Material"
            PropertyChanges {
                target: toolBarLoader
                source: "ToolbarMaterial.qml"
            }
        },
        State {
            name: "Magazine"
            PropertyChanges {
                target: toolBarLoader
                source: "ToolbarMagazine.qml"
            }
        },
        State {
            name: "Job"
            PropertyChanges {
                target: toolBarLoader
                source: "ToolbarJob.qml"
            }
        },
        State {
            name: "Statistics"
            PropertyChanges {
                target: toolBarLoader
                source: "ToolbarStatistics.qml"
            }
        },
        State {
            name: "Service"
            PropertyChanges {
                target: toolBarLoader
                source: "ToolbarService.qml"
            }
        }
        // @State@
    ]


    ErrorSpinner {
        id: error_widget1
        x: 310
        y: 8
        width: 564
        height: 76
        showMessages: true
        showErrors: true
        errorModelPath: "ErrorListModel.qml"
        __errorString: "Error_i"


    }

    StyleButton {
        id: error_reset
        x: 880
        y: 10
        width: 76
        height: 76
        //text: "Reset Error"
        keyBinding: "Alt+F1"
        opacity: !Machine_bEnable.value || Error_NonCritical_Out_bActive.value || Error_Critical_Out_bActive.value ? 1 : 0
        onClicked: {
            Machine_bEnable.value = true
            Error_bReset.value = true
        }
        __imageNormal: imagePath + "ErrorReset.png"
        __imagePressed: imagePath + "ErrorReset.png"
    }

}
