#pragma once

#include <QString>
#include <map>
#include "die_coord.h"
#include "die_bounds.h"
#include "mathfunctions.h"


// Types
//		Line, Arc, Text

class element
{
protected:

	typedef std::map< int, int >			intsType;
	typedef std::map< int, Die::Coord >		coordsType;
	typedef std::map< int, double >			doublesType;
    typedef std::map< int, QString >		StringsType;
    typedef std::map< int, int >			intId;


	int				_type;
	double			_length;
	coordsType		_coords;
	intsType		_ints;
	doublesType		_doubles;
	StringsType		_strings;
	bool			_isHighlighted;
	int				_materialID;
	int				_color;
public:
	element(void);
	~element(void);


	void setLength(double);
	void setType( int );
	void setCoord( int, const Die::Coord& );
	void setInt( int, int );
	void setDouble( int, double );
    void setText( int, QString);
    void setHighlight(bool h){ _isHighlighted = h; }
    bool getHighlight(){ return _isHighlighted; }
    int	 getMaterialID(){ return _materialID; }
    void setMaterialID(int id){ _materialID = id; }
    int	 getColor(){ return _color; }
    void setColor(int color	){ _color = color; }

	double			getLength();
	int				getType();
	int				getInt( int );
	double			getDouble( int );
	Die::Coord		getCoord( int );
    QString			getString( int );

	Die::Bounds		getBoundingBox();

	double			getDistance( const Die::Coord& );
	Die::Coord		lambda( double );
	Die::Angle		getAngle( );

    enum ET{ ET_LINE, ET_ARC, ET_TEXT, ET_TOOL, ET_MACRO };
    enum CID { CID_START, CID_END, CID_CENTER };
    enum DID { DID_BRIDGELENGTH, DID_POINTAGE, DID_AUXTYPE, DID_ARCANGLE, DID_ZCOORD};
    enum IID { IID_BRIDGENUM, IID_LINETYPE, IID_TOOL, IID_ID, IID_LINENUM };
    enum SID { SID_TEXT, SID_MACRO, SID_LINETYPE, SID_LINEWIDTH, SID_LAYER, SID_LINECOLOR};
};

