#pragma once
#include "mathfunctions.h"

namespace Die
{

	// Deg /////////////////////////////////////////////////////////////////////

	Deg::Deg()
	: m_radian(0)
	{}


	Deg::Deg(double degree)
	{ 
		m_radian=degree/Die_radian; 
	}


	double
	Deg::radian() const
	{
		return m_radian;
	}



	// Rad /////////////////////////////////////////////////////////////////////

	Rad::Rad()
	: m_radian(0)
	{}


	Rad::Rad(double radian)
	{ 
		m_radian=radian;
	}
	

	double
	Rad::degree() const
	{
		return m_radian*Die_radian;
	}



	// FreeAngle ///////////////////////////////////////////////////////////////

	FreeAngle::FreeAngle()
	: m_radian(0)
	{}


	FreeAngle::FreeAngle(const FreeAngle& a0)
	: m_radian(a0.m_radian)
	{}


	FreeAngle::FreeAngle(const Deg& a0) 
	: m_radian(a0.m_radian)
	{}


	FreeAngle::FreeAngle(const Rad& a0)
	: m_radian(a0.m_radian)
	{}


	FreeAngle::FreeAngle(const Angle& a0)
	: m_radian(a0.m_radian)
	{}

	FreeAngle::FreeAngle(const Coord& c)
	{
		switch (c.x==0 ? 0 : (c.x>0 ? 1 : -1)) {
		case 0:	switch (c.y==0 ? 0 : (c.y>0 ? 1 : -1)) {
					case 0: m_radian=0; break;
					case 1: m_radian=Die_pi2; break;
					case -1: m_radian=Die_3pi2; break;
				}
				break;
		case 1:
		case -1: m_radian=atan2(c.y,c.x); break;
		}
	}


	double
	FreeAngle::radian() const 
	{
		return m_radian;
	}


	double
	FreeAngle::degree() const 
	{
		return m_radian*Die_radian;
	}


	FreeAngle& 
	FreeAngle::operator=(const FreeAngle& a0)
	{
		m_radian=a0.m_radian;
		return *this;
	}


	FreeAngle& 
	FreeAngle::operator+=(const FreeAngle& a0) 
	{
		m_radian+=a0.m_radian;
		return *this;
	}


	FreeAngle& 
	FreeAngle::operator-=(const FreeAngle& a0)
	{
		m_radian-=a0.m_radian;
		return *this;
	}


	FreeAngle& 
	FreeAngle::operator*=(const double& l) 
	{
		m_radian*=l;
		return *this;
	}


	FreeAngle& 
	FreeAngle::operator/=(const double& l) 
	{
		m_radian/=l;
		return *this;
	}


	FreeAngle
	operator+(const FreeAngle& a1,const FreeAngle& a2) 
	{
		FreeAngle a;
		a.m_radian=a1.m_radian+a2.m_radian;
		return a;
	}


	FreeAngle
	operator-(const FreeAngle& a1,const FreeAngle& a2) 
	{
		FreeAngle a;
		a.m_radian=a1.m_radian-a2.m_radian;
		return a;
	}


	FreeAngle
	operator-(const FreeAngle& a0)
	{
		FreeAngle a;
		a.m_radian=-a0.m_radian;
		return a;
	}


	FreeAngle
	operator*(const FreeAngle& a0,const double& l) 
	{
		FreeAngle a;
		a.m_radian=a0.m_radian*l;
		return a;
	}


	FreeAngle
	operator*(const double& l,const FreeAngle& a0) 
	{
		FreeAngle a;
		a.m_radian=a0.m_radian*l;
		return a;
	}


	FreeAngle
	operator/(const FreeAngle& a0,const double& l) 
	{
		FreeAngle a;
		a.m_radian=a0.m_radian/l;
		return a;
	}


	void 
	FreeAngle::round( const int N )
	{
		double d = degree();
		d *= pow( 10.0, N );
		d = int( d + (d < 0 ? -0.5 : 0.5 ) );
		d /= pow( 10.0, N );
		m_radian = d / Die_radian;
	}

	// Angle ///////////////////////////////////////////////////////////////////

	Angle::Angle()
	: m_radian(0)
	{}


	Angle::Angle(const Coord& c) 
	{
		switch (c.x==0 ? 0 : (c.x>0 ? 1 : -1)) {
		case 0:	switch (c.y==0 ? 0 : (c.y>0 ? 1 : -1)) {
					case 0: m_radian=0; break;
					case 1: m_radian=Die_pi2; break;
					case -1: m_radian=Die_3pi2; break;
				}
				break;
		case 1:
		case -1: m_radian=atan2(c.y,c.x); break;
		}
		normalize();
	}


	Angle::Angle(const FreeAngle& a0)
	: m_radian(a0.m_radian) 	
	{
		normalize();
	}


	Angle::Angle(const Deg& a0)
	: m_radian(a0.m_radian) 
	{
		normalize();
	}


	Angle::Angle(const Rad& a0)
	: m_radian(a0.m_radian) 
	{
		normalize();
	}


	Angle::Angle(const Angle& a0)
	: m_radian(a0.m_radian)
	{}


	double
	Angle::radian() const {
		return m_radian;
	}


	double
	Angle::degree() const {
		return m_radian*Die_radian;
	}


	Angle&
	Angle::operator+=(const FreeAngle& a0) {
		m_radian+=a0.m_radian;
		normalize();
		return *this;
	}


	Angle&
	Angle::operator-=(const FreeAngle& a0) {
		m_radian-=a0.m_radian;
		normalize();
		return *this;
	}


	Angle&
	Angle::operator*=(const double& l) {
		m_radian*=l;
		normalize();
		return *this;
	}


	Angle&
	Angle::operator/=(const double& l) {
		m_radian/=l;
		normalize();
		return *this;
	}


	void
	Angle::normalize() {
		while (m_radian<0) { m_radian+=Die_2pi; }
		while (m_radian>=Die_2pi) { m_radian-=Die_2pi; }
	}


	Angle
	Angle::conjugate() const
	{
		Angle	angle;

		angle.m_radian=Die_2pi-m_radian;
		return angle;	
	}


	Angle
	operator-(const Angle& a0)
	{
		Die::Angle a;
		return (a-=a0);
	}


	bool
	operator==(const Angle& a0, const Angle& a1)
	{
		Die::Angle a(a0-a1);
		a+=Rad(Die_zero);
		return a.radian()<2*Die_zero;
	}


	bool
	operator!=(const Angle& a0, const Angle& a1)
	{
		return !(a0==a1);
	}


	Angle
	bisector(const Angle& s, const Angle& e)
	{
		return (e-s)/2+s;
	}



	// Coord ///////////////////////////////////////////////////////////////////

	Coord::Coord()
	: x(0), y(0)
	{
	}


	Coord::Coord(const double& x0,const double& y0)
	: x(x0),y(y0)
	{
	}


	Coord::Coord(const Coord& c0)
	: x(c0.x), y(c0.y)
	{
	}


	Coord::Coord(const FreeAngle& a0)
	: x(cos(a0.radian())), y(sin(a0.radian()))
	{
	}


	Coord::Coord(const double& z0, const FreeAngle& a0)
	: x(z0*cos(a0.radian())), y(z0*sin(a0.radian()))
	{
	}


	Coord& 
    Coord::operator=( QString& S ) {
        int	pos = S.indexOf(";");

        x = strToDbl( S.left( pos ) );
        y = strToDbl( S.mid( pos+1 ) );

        return *this;
	}

/*
    Coord&
	Coord::operator=( TCHAR* S ) {
		TCHAR*	pos = _tcsstr(  S, _T(";") );

		y = strToDbl( pos+1 );
		x = strToDbl( S );

		return *this;
	}
*/

	Coord&
	Coord::operator=(const Coord& c0) {
		x=c0.x, y=c0.y; 
		return *this;
	}


	Coord&
	Coord::operator+=(const Coord& c0) { 
		x+=c0.x, y+=c0.y; 
		return *this;
	}


	Coord&
	Coord::operator-=(const Coord& c0) {
		x-=c0.x, y-=c0.y; 
		return *this;
	}


	Coord&
	Coord::operator*=(const double& l) {
		x*=l, y*=l; 
		return *this;
	}


	Coord&
	Coord::operator/=(const double& l) {
		x/=l, y/=l; 
		return *this;
	}


	Coord
	Coord::rotate(const Coord& u) const 
	{
		return Die::Coord(u.x*x-u.y*y,u.y*x+u.x*y);
	}


	Coord
	Coord::conjugate() const 
	{
		return Die::Coord(x,-y);
	}


	double
	Coord::norm() const 
	{
		return sqrt(x*x+y*y);
	}


	double
	Coord::norm_square() const
	{
		return (x*x+y*y);
	}


	Coord
	Coord::normalize() const 
	{
		double d=sqrt(x*x+y*y);
		if (d>0) { return Coord(x/d,y/d); }
		else { return *this; }
	}


	Coord
	Coord::normalize(const double& n) const 
	{
		double d=sqrt(x*x+y*y);
		if (d>0) { d=n/d; return Coord(x*d,y*d); }
		else { return *this; }
	}


	Coord 
	Coord::rnormal() const 
	{ 
		return Coord(y,-x);
	}


	Coord 
	Coord::lnormal() const 
	{ 
		return Coord(-y,x);
	}


	void
	rotate(Coord& a, const Coord& u) 
	{
		double ax=a.x;
		a.x=u.x*ax-u.y*a.y;
		a.y=u.y*ax+u.x*a.y;
	}


	bool
	operator==(const Coord& c1,const Coord& c2) 
	{
		return (fabs(c1.x-c2.x)<Die_zero && fabs(c1.y-c2.y)<Die_zero);
	}


	bool
	operator!=(const Coord& c1,const Coord& c2) 
	{
		return !(c1==c2);
	}



	Coord
	operator+(const Coord& c1,const Coord& c2) 
	{
		Coord c(c1.x+c2.x,c1.y+c2.y);
		return c;
	}



	Coord
	operator-(const Coord& c1,const Coord& c2) 
	{
		Coord c(c1.x-c2.x,c1.y-c2.y);
		return c;
	}

	
	Coord
	operator-(const Coord& c0)
	{
		Coord c(-c0.x,-c0.y);
		return c;
	}

	 
	Coord
	operator*(const Coord& c0, const double& l) 
	{
		Coord c(c0.x*l,c0.y*l);
		return c;
	}


	Coord
	operator*(const double& l, const Coord& c0) 
	{
		Coord c(c0.x*l,c0.y*l);
		return c;
	}


	Coord
	operator/(const Coord& c0, const double& l)
	{
		Coord c(c0.x/l,c0.y/l);
		return c;
	}
	

	//  v,u		a k�t szakaszvektor, a v kezdopontja szamit origonak
	//  q		az u szakasz kezd�pontja v szakaszhoz kezd�pontj�hoz k�pest
	//  lambda	ha a k�t szakasz �ltal meghat�rozott egyenes metszi egym�st, akkor a metsz�spont: q+u*lamda
	//			0 <= lambda <=  1 eset�n az _egyenesek_ metsz�spontja az u szakaszra esik
	//			ha a k�t szakasz p�rhuzamos, akkor lambda == 0.
	double 
	lambda(Coord v, Coord q, Coord u)
	{
		if (v.x==0) {
			if (u.x!=0) { return -q.x/u.x; }
			else { return 0; }
		}
		else {
			q.x/=v.x, q.y-=q.x*v.y, q.y/=v.x;
			u.x/=v.x, u.y-=u.x*v.y, u.y/=v.x;
			if (u.y!=0) { return -q.y/u.y; }
			else { return 0; }
		}
	}


	bool
	parallel(const Coord& v, const Coord& u)
	{
		double	un=u.norm();
		double	vn=v.norm();
		double	c=cross(v, u);

		return (un<Die_zero || vn<Die_zero || (fabs(c/un)<Die_zero && fabs(c/vn)<Die_zero));
	}


	double
	cross(const Coord& p, const Coord& q)	// cross product
	{
		return p.x*q.y-p.y*q.x;
	}



	// Base ////////////////////////////////////////////////////////////////////

	Base::Base()
	: x(1, 0)
	, y(0, 1)
	{}


	Base::Base(const Coord& x0, const Coord& y0)
	: x(x0)
	, y(y0)
	{}


	Coord 
	Base::transform(const Coord& coord) const
	{
		return coord.x*x + coord.y*y;
	}


	Base
	Base::transform(const Base& base0) const
	{
		Base	base1;

		base1.x=transform(base0.x);
		base1.y=transform(base0.y);
	}

 
	Base
	Base::invert() const
	{
		Base		base;
		double		e=x.x*y.y - x.y*y.x;

		if (e!=0) {
			e=1/e;
			base.x=Die::Coord(e*y.y, -e*x.y);
			base.y=Die::Coord(-e*y.x, e*x.x);
		}
		return base;
	}



} // namespace end
