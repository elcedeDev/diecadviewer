#include "diecadviewer.h"

#include <QPainter>
#include <QDir>
#include <QList>
#include <QGraphicsSceneMouseEvent>
#include "importACC.h"
#include "importCNC.h"
#include "importHPGL.h"
//#include "importCF2.h"
#include "element.h"
#include <QApplication>
#include <QTime>
#include <QtMath>
//#include <QDesktopServices>
//#include <QProcess>
#include "version.h"
#include <QFileDialog>


DieCadViewer::DieCadViewer(QDeclarativeItem *parent):
    QDeclarativeItem(parent)
{
    // need to disable this flag to draw inside a QDeclarativeItem

    setFlag(QGraphicsItem::ItemHasNoContents, false);


    setAcceptedMouseButtons(Qt::LeftButton | Qt::RightButton | Qt::MiddleButton);
    setEnabled(true);
    //grabMouse();
    setKeepMouseGrab(true);


    for(int i = 0; i < 41; ++i)
    {
        m_tools[i].color = QColor();
        m_tools[i].width = 1;
    }
    m_tools[0].color = QColor(Qt::gray);
    m_tools[1].color = QColor(Qt::red);
    m_tools[2].color = QColor(Qt::blue);
    m_tools[3].color = QColor(Qt::cyan);
    m_tools[4].color = QColor(Qt::green);
    m_tools[5].color = QColor(Qt::magenta);
    m_tools[6].color = QColor(255,134,53); //brown
    m_tools[7].color = QColor(Qt::gray);
    m_tools[8].color = QColor(Qt::blue).light();
    m_tools[9].color = QColor(Qt::green).light();
    m_tools[10].color = QColor(Qt::cyan).light();
    m_tools[11].color = QColor(Qt::red).light();
    m_tools[12].color = QColor(Qt::magenta).light();
    m_tools[13].color = QColor(Qt::yellow);
    m_tools[14].color = QColor(200,255,255); //blue.lightness();
    m_tools[15].color = QColor(Qt::darkMagenta);
    m_tools[16].color = QColor(198,97,90); //brown light
    m_tools[17].color = QColor(Qt::magenta);
    m_tools[18].color = QColor(255,134,53).light();
    m_tools[19].color = QColor(255,134,53).lighter() ;
    m_tools[20].color = QColor(Qt::blue).darker();
    m_tools[20].color = QColor(Qt::red).darker();
    m_tools[21].color = QColor(Qt::red);
    m_tools[22].color = QColor(Qt::blue);
    m_tools[23].color = QColor(Qt::cyan);
    m_tools[24].color = QColor(Qt::green);
    m_tools[25].color = QColor(Qt::magenta);
    m_tools[26].color = QColor(255,134,53); //brown
    m_tools[27].color = QColor(Qt::gray);
    m_tools[28].color = QColor(Qt::blue).light();
    m_tools[29].color = QColor(Qt::green).light();
    m_tools[30].color = QColor(Qt::cyan).light();
    m_tools[31].color = QColor(Qt::red).light();
    m_tools[32].color = QColor(Qt::magenta).light();
    m_tools[33].color = QColor(Qt::yellow);
    m_tools[34].color = QColor(Qt::blue).lighter();
    m_tools[35].color = QColor(Qt::darkMagenta);
    m_tools[36].color = QColor(198,97,90); //brown light
    m_tools[37].color = QColor(Qt::magenta);
    m_tools[38].color = QColor(255,134,53).light();
    m_tools[39].color = QColor(255,134,53).lighter() ;
    m_tools[40].color = QColor(Qt::blue).darker();


    m_toolIndex = 0;
    m_toolShow = QBitArray(41);
    m_toolShow.fill(true);
    m_isLoaded = false;

    m_toolShow[0] = false;

    int   arrowSize = 100;

    QVector<QPoint>   arrowPoints;
    QPoint  p = QPoint(0,0);
    arrowPoints.append(p);
            p = QPoint(-arrowSize,- arrowSize /2);
    arrowPoints.append(p);
            p = QPoint(-arrowSize, arrowSize /2);
    arrowPoints.append(p);
            p = QPoint(0, 0);
    arrowPoints.append(p);
    //create arrow head symbol as path
    m_arrowDirection.addPolygon(QPolygon(arrowPoints));


    qreal headSize = 25;
    QPointF point = QPointF(0,0);
    //create machine head symbol as path
    m_machineHead.moveTo(point.x() - headSize, point.y());
    m_machineHead.lineTo(point.x() + headSize, point.y());
    m_machineHead.moveTo(point.x(), point.y() - headSize);
    m_machineHead.lineTo(point.x(), point.y() + headSize);
    m_machineHead.moveTo(point.x(), point.y());
    m_machineHead.arcTo(point.x() - headSize,
               point.y() - headSize,
               2*headSize, 2*headSize, 0.0, 360.0);


    //arrowPointsV.clear();
    arrowPoints.clear();

    m_offset = QPointF(0,0);
    m_mousepos = QPointF(0,0);

    m_totalMoves = 0.0;
    m_emptyMoves = 0.0;

    m_totalMovesName = "Dist";
    m_emptyMovesName = "Empty";
    m_movesDimension = "m";

    m_min = QPointF(0.0, 0.0);
    m_max = QPointF(0.0, 0.0);

    m_movesFactor = 0.001;

    m_selectedColor = QColor(Qt::magenta);
    m_processedColor = QColor(Qt::black);
    m_textColor = QColor(Qt::black);

    m_actualElement = -1;
    m_selectedElement = -1;

    m_textMargin = 4;

    m_fileErrorText = "ERROR\nFILE NOT FOUND\n OR NOT SUPPORTED FORMAT";
    m_fileLoadingText = "Loading file...";
    m_bNeedRefresh = true;

    m_timer = startTimer(1000);   // 1-second timer
    m_verbose = false;
    m_tapTime.start();
    m_tapDoubleClickTime = 250;
    m_rotationAngle = 90;
    m_rotationCenter = QPointF(0,0);

    QObject::connect(&m_watcher, SIGNAL(fileChanged(QString)), this, SLOT(fileModified(QString)));

    //QObject::connect(this , SIGNAL(bKillExecuteCmdChanged()), this, SLOT(killExecuteCmd()));

    //selected gestures
    QList<Qt::GestureType> gestures;
    gestures  << Qt::PinchGesture << Qt::TapGesture << Qt::PanGesture;
    grabGestures(gestures);

    m_backgroundColor = QColor(Qt::white);

    m_machineXlength  = 2000.0;
    m_machineYlength  = 4000.0;

    m_machineSize = QSizeF(2000.0, 4000.0);

    m_zeroPoint   = QPointF(0.0,0.0);
    //m_machineRect = QRectF(QPointF(0.0,0.0), QSizeF(m_machineXlength,-m_machineYlength));
    m_machineRect = QRectF(0.0,0.0, m_machineSize.width(), -m_machineSize.height());

    m_machinePen  = QPen(QBrush(QColor(Qt::red)), 4);

    m_showMachine = true;
    m_machinePosition = m_zeroPoint;

    QTime ct = QTime::currentTime();
    ct = ct.addSecs(-2);
    m_zeroPositionLastModified = ct;

    m_engravingTool = -1;
    m_IsEngraving = false;
    m_isJobMoveAllowed = true;

    m_flangePosY[0] = 0;
    m_flangePosY[1] = 620;
    m_flangePosY[2] = 1052;
    m_flangePosY[3] = 1702;
    m_flangePosY[4] = 2102;
    m_flangePosY[5] = 2502;
    m_flangePosY[6] = 2962;
    m_flangeColor = QColor(Qt::yellow);

    m_machineMargin = 5;
    m_flangeWidth = 10;
    m_numofElem = 0;

    m_isFileCheckedRegularly = false;
    m_isRotaMate    = false;

    m_fileOpenFilter = "CAD Files (*.nc *.acc *.cf2 *.dxf *.plt *. hpgl *.hgl *.rcc *.rcf *.cnc *N)";

    PRINTVERSION
}

DieCadViewer::~DieCadViewer()
{
    if (m_elements.size())
        for (std::map< int, element* >::iterator it = m_elements.begin(); it != m_elements.end(); ++it){
            element*		E = it->second;
            delete E;
        }
    m_elements.clear();
    //m_toolColors.clear();
    m_tools.clear();
    m_toolShow.clear();
    m_elementPath.clear();
    killTimer(m_timer);
}

QString DieCadViewer::filename() const
{
    return m_filename;
}

void DieCadViewer::setFileName(const QString &filename)
{

    if(!m_nativefilename.isEmpty())
        m_watcher.removePath(m_nativefilename);

    m_filename = filename;
    if(m_filename != "")
    {
        QUrl url;
        //for backward compatibility: files: added in qml code by mistaken remove it
        if(m_filename.contains("files:"))
        {
            url = QUrl::fromLocalFile( m_filename.mid(6,-1));
        }
        else if(m_filename.contains("file:///"))
        {
            url = QUrl::fromLocalFile( m_filename.mid(8, -1));
        }
        else
        {
            url = QUrl::fromLocalFile( m_filename );
        }
        m_nativefilename = convertUrlToNativeFilePath(url);
        /*
        qDebug() << "m_filename:" << m_filename;
        qDebug() << "m_filename.mid(6,-1)" << m_filename.mid(6,-1);
        qDebug() << "url:" << url;
        qDebug() << "m_nativefilename:" << m_nativefilename;
        */
    }
    else
        m_nativefilename = m_filename;

    if(!m_nativefilename.isEmpty())
        m_watcher.addPath(m_nativefilename);
    refresh();
    emit filenameChanged();
}


QString DieCadViewer::convertUrlToNativeFilePath(const QUrl& urlStylePath) const
{
    return QDir::toNativeSeparators(urlStylePath.toLocalFile());
}


QString DieCadViewer::totalMovesName() const
{
    return m_totalMovesName;
}

void DieCadViewer::setTotalMovesName(const QString &totalMovesName)
{
    m_totalMovesName = totalMovesName;
}

QString DieCadViewer::emptyMovesName() const
{
    return m_emptyMovesName;
}

void DieCadViewer::setEmptyMovesName(const QString &emptyMovesName)
{
    m_emptyMovesName = emptyMovesName;
}

QString DieCadViewer::movesDimension() const
{
    return m_movesDimension;
}

void DieCadViewer::setMovesDimension(const QString &movesDimension)
{
    m_movesDimension = movesDimension;
}

qreal DieCadViewer::scaleFactor() const
{
    return m_Zoom;
}

void DieCadViewer::setScaleFactor(const qreal factor)
{
    m_Zoom = factor;
    if(m_showMachine)
        m_machine_Zoom = m_Zoom;
    else
        m_normal_Zoom = m_Zoom;
}

QPointF DieCadViewer::position() const
{
    return m_offset;
}

void DieCadViewer::setPosition(const QPointF p)
{
    m_offset = p;
    update();
    if(m_showMachine)
        m_machine_Offset = p;
    else
        m_normal_Offset = p;
}

qreal DieCadViewer::movesFactor() const
{
    return m_movesFactor;
}

void DieCadViewer::setMovesFactor(const qreal movesFactor)
{
    m_movesFactor = movesFactor;
}

int DieCadViewer::selectedElem() const
{
    return m_selectedElement;
}

void DieCadViewer::setSelectedElem(int iElem)
{
    m_selectedElement = iElem;
}

int DieCadViewer::actualElem() const
{
    return m_actualElement;
}

void DieCadViewer::setActualElem(int iElem)
{
    m_actualElement = iElem;
    update();
}

QColor DieCadViewer::textColor() const
{
    return m_textColor;
}

void DieCadViewer::setTextColor(const QColor &color)
{
    m_textColor = color;
    update();
}

QColor DieCadViewer::backgroundColor() const
{
    return m_backgroundColor;
}

void DieCadViewer::setBackgroundColor(const QColor &C)
{
    m_backgroundColor = C;
}

QColor DieCadViewer::processedColor() const
{
    return m_processedColor;
}

void DieCadViewer::setProcessedColor(const QColor &color)
{
    m_processedColor = color;
    update();

}

QString DieCadViewer::fileErrorText() const
{
    return m_fileErrorText;
}

void DieCadViewer::setFileErrorText(const QString &str)
{
    m_fileErrorText = str;
}

qreal DieCadViewer::minX() const
{
   return  m_min.x()* 0.001; //return in m
}

void DieCadViewer::setMinX(const qreal)
{
    return;//readonly
}

qreal DieCadViewer::minY() const
{
    return  m_min.y() * 0.001; //return in m
}

void DieCadViewer::setMinY(const qreal)
{
    return;//readonly
}

qreal DieCadViewer::maxX() const
{
    return  m_max.x()* 0.001; //return in m
}

void DieCadViewer::setMaxX(const qreal)
{
    return;//readonly
}

qreal DieCadViewer::maxY() const
{
    return  m_max.y()* 0.001; //return in m
}

void DieCadViewer::setMaxY(const qreal)
{
    return;//readonly
}

QPointF DieCadViewer::zeroPosition_In() const
{
    return m_zeroPoint/1000.0;
}

void DieCadViewer::setZeroPosition_In(const QPointF p)
{
    int t = m_zeroPositionLastModified.msecsTo(QTime::currentTime());
    //qDebug() << "-------- DieCadViewer::setZeroPosition_In(const QPointF p) -----: t" << t << "m_zeroPositionLastModified" << m_zeroPositionLastModified;
    if( t > 2000 ){
        m_zeroPoint = p*1000.0; //QPoint(p.x(), p.y());
        m_zeroPoint.setY(m_zeroPoint.y()*-1);
        m_zeroPosition_In = p;
        update();
        if(m_showMachine)
            m_machine_ZeroPoint = m_zeroPoint;
        else
            m_normal_ZeroPoint = m_zeroPoint;
        //qDebug() << "-------- m_zeroPoint -----:" << m_zeroPoint;
    }
}

QPointF DieCadViewer::zeroPosition_Out() const
{
    return QPointF(m_zeroPoint.x()/1000.0, -m_zeroPoint.y()/1000.0);
}

QPointF DieCadViewer::machinePosition() const
{
    return m_machinePosition/1000.0;
}

void DieCadViewer::setMachinePosition(const QPointF p)
{
    m_machinePosition = p*1000.0;
    update();
    //qDebug() << "-------- m_machinePosition -----:" << m_machinePosition;
}

QSizeF DieCadViewer::machineSize() const
{
    return m_machineSize / 1000.0;
}

void DieCadViewer::setMachineSize(const QSizeF s)
{
    m_machineSize = s*1000.0;
    m_machineRect = QRectF(0.0,0.0,m_machineSize.width(), -m_machineSize.height());
    if(m_showMachine){
        zoomAll();
        update();
        //qDebug() << "-------- m_machineSize -----:" << m_machineSize;
    }
}

bool DieCadViewer::showMachine() const
{
    return m_showMachine;
}

void DieCadViewer::setShowMachine(const bool b)
{
    m_showMachine = b;
    if(m_showMachine){
        m_Zoom = m_machine_Zoom;
        m_offset = m_machine_Offset ;
        m_zeroPoint = m_machine_ZeroPoint;
    }
    else
    {
        m_Zoom = m_normal_Zoom;
        m_offset = m_normal_Offset ;
        m_zeroPoint = m_normal_ZeroPoint;
    }

    update();
}

int DieCadViewer::engravingTool() const
{
    return m_engravingTool;
}

void DieCadViewer::setEngravingTool(const int iTool)
{
    m_engravingTool = iTool;
}

bool DieCadViewer::isEngraving() const
{
    return m_IsEngraving;
}

bool DieCadViewer::isJobMoveAllowed()
{
    return m_isJobMoveAllowed;
}

void DieCadViewer::setisJobMoveAllowed(const bool bVal)
{
    m_isJobMoveAllowed = bVal;
}

qreal DieCadViewer::rotationAngle() const
{
    return m_rotationAngle;
}

void DieCadViewer::setRotationAngle(const qreal fVal)
{
    m_rotationAngle = fVal;
    update();
}

QPointF DieCadViewer::rotationCenter() const
{
    return m_rotationCenter;
}

void DieCadViewer::setRotationCenter(const QPointF p)
{
    m_rotationCenter = p;
    update();
}


void DieCadViewer::grabGestures(const QList<Qt::GestureType> &gestures)
{
    foreach (Qt::GestureType gesture, gestures)
            grabGesture(gesture);
}


bool DieCadViewer::gestureEvent(QGestureEvent *event)
{
    //qDebug() << "---- DieCadViewer::gestureEvent(QGestureEvent *event) ----";
    if (QGesture *pinch = event->gesture(Qt::PinchGesture)){
        pinchTriggered(static_cast<QPinchGesture *>(pinch), event);
    }else if (QGesture *pan = event->gesture(Qt::PanGesture)){
        panTriggered(static_cast<QPanGesture *>(pan));
    }else if (QGesture *tap = event->gesture(Qt::TapGesture)){
        tapTriggered(static_cast<QTapGesture *>(tap));
    }
    return true;
}

bool DieCadViewer::isRotationUsed() const
{
    return m_isRotationUsed;
}

void DieCadViewer::setisRotationUsed(const bool b)
{
    m_isRotationUsed = b;
    zoomAll();
    update();
}

QPointF DieCadViewer::offset() const
{
    return m_offset;
}

void DieCadViewer::setOffset(const QPointF p)
{
    m_offset = p;
    update();
}

void DieCadViewer::fileModified(const QString &str)
{
    Q_UNUSED(str);
    m_isFileModified = true;
    //checkFile();
    //if(m_bNeedRefresh) refresh();
}

void DieCadViewer::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    //qDebug() << "DieCadViewer::wheelEvent(QGraphicsSceneWheelEvent *event) " <<  event->type() << "event->pos()" << event->pos() << "event->delta()" << event->delta() << "Qt::ControlModifier == QApplication::keyboardModifiers()" << (Qt::ControlModifier == QApplication::keyboardModifiers());
    if(Qt::ControlModifier == QApplication::keyboardModifiers() ){
/*
        m_rotationCenter =  event->pos();

        if (event->delta() > 0)
            m_rotationAngle += 10;
        else
            m_rotationAngle -= 10;
*/
    }else{

        double scale0 = m_Zoom ;
        double scale;

        QPointF c0 = S2C( event->pos() );


        if (event->delta() > 0)
            scale = scale0 * 1.2;
        else
            scale = scale0 / 1.2;

        QPointF c1;

        c1 = c0 * (scale - scale0);

        m_offset += c1*-1.0;

        m_Zoom = scale;

        if(m_showMachine){
            m_machine_Zoom = m_Zoom;
            m_machine_Offset = m_offset;
        }
        else
        {
            m_normal_Zoom = m_Zoom;
            m_normal_Offset = m_offset;
        }
        m_mousepos = event->pos();

    }

    update();
}



void DieCadViewer::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QDeclarativeItem::mouseMoveEvent(event);

    if(m_drag && !m_showMachine){
        if(m_isRotationUsed) setRotation(m_rotationAngle);

        m_offset.setX( m_offset.x() + mapFromScene(event->screenPos()).x() - mapFromScene(event->lastScreenPos()).x());
        m_offset.setY( m_offset.y() + mapFromScene(event->lastScreenPos()).y() - mapFromScene(event->screenPos()).y());

        if(m_isRotationUsed) setRotation(0);

        m_normal_Offset = m_offset;

    }
    else if(m_drag && m_showMachine && m_isJobMoveAllowed)
    {
        //zoomAll();
        if(m_isRotationUsed) setRotation(m_rotationAngle);

        bool  bDontMove = false;
        QPointF p = QPointF(m_zeroPoint.x() + (mapFromScene(event->screenPos()).x() - mapFromScene(event->lastScreenPos()).x())/m_Zoom,
                          m_zeroPoint.y() + (mapFromScene(event->screenPos()).y() - mapFromScene(event->lastScreenPos()).y())/m_Zoom);

        if(   (m_machineRect.x() > p.x())
            ||(m_machineRect.y() < p.y())
            ||(m_machineRect.topRight().x() < (p.x() + m_jobRect.topRight().x()))
            ||(fabs(m_machineRect.bottomRight().y()) < (fabs(p.y()) + m_jobRect.bottomRight().y())) )  bDontMove = true;

        if(! bDontMove) m_zeroPoint = p;

        if(m_isRotationUsed) setRotation(0);
        m_machine_Offset = m_offset;
        m_machine_ZeroPoint = m_zeroPoint;
        m_zeroPositionLastModified = QTime::currentTime();
        emit zeroPositionChanged();
    }
    update();
}

void DieCadViewer::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if( event->button() == Qt::MiddleButton){
        m_drag = true;
        return;
    }
    if( !m_drag && event->button() == Qt::LeftButton){

        if(Qt::ControlModifier == QApplication::keyboardModifiers() && m_selectedElement > -1)
        {
            if(int(m_elements.size() - 1) > m_selectedElement)  m_selectedElement +=1;
            update();
            return;
        }

        QPointF p = QPointF(event->pos());
        QPointF p2;
        QPointF p3;
        if(m_showMachine){
            if(m_isRotationUsed){
                QPointF pt1 = p - m_rotationCenter;
                p2          = QPointF(pt1.y(),pt1.x()*-1);//-90 fok
                p           = p2 + m_rotationCenter;
            }
            p2 = QPointF(p.x() - m_offset.x(),-1.0*(p.y()+ m_offset.y() + m_machineRect.height()* m_Zoom ))/m_Zoom;
            p3 = QPointF(p2.x() - m_zeroPoint.x(), p2.y() + m_zeroPoint.y());
            selectElem(p3);
            return;

        }
        else
            selectElem(S2C(p));

        //qDebug() << "p:" << p << "m_zeroPoint:" << m_zeroPoint << "m_offset:" << m_offset << "p - m_zeroPoint:" << (p - m_zeroPoint)  << "m_Zoom:" << m_Zoom << "m_machineRect:" << m_machineRect << "p2:" << p2 << "p3:" << p3 << "S2C(p):" << S2C(p);

        return;
    }

    if( !m_drag && event->button() == Qt::RightButton){

        if(Qt::ControlModifier == QApplication::keyboardModifiers() && m_selectedElement > 1)
        {
            m_selectedElement -=1;
            update();
            return;
        }
    }
}

void DieCadViewer::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if( event->button() == Qt::MiddleButton){
        m_drag = false;
        //qDebug() << "mouseReleaseEvent";
    }

}

void DieCadViewer::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if(Qt::ControlModifier == QApplication::keyboardModifiers()) return;

    //refresh();
    if( event->button() == Qt::MiddleButton || event->button() == Qt::LeftButton){
        m_drag = false;

        zoomAll();
        update();
    }
}



void DieCadViewer::pinchTriggered(QPinchGesture *gesture, QGestureEvent *event)
{
    QPinchGesture::ChangeFlags changeFlags = gesture->changeFlags();
    QPointF screen = gesture->centerPoint();
    screen = mapFromScene(event->mapToGraphicsScene(gesture->centerPoint()));

    if (changeFlags & QPinchGesture::RotationAngleChanged) {
     /* --- commented out requested by SB
        if(gesture->rotationAngle() != gesture->lastRotationAngle()){
            const qreal value = gesture->rotationAngle();
           // const qreal lastValue = gesture->lastRotationAngle();
            const qreal rotationAngleDelta = value - gesture->lastRotationAngle();;
            m_rotationCenter = screen;
            m_rotationAngle += rotationAngleDelta;
            //update();

        }
        if (m_verbose)
            qDebug() << "pinchTriggered(): m_rotationAngle" << m_rotationAngle << "gesture->lastRotationAngle() " << gesture->lastRotationAngle() << "gesture->rotationAngle() " << gesture->rotationAngle();
      */
    }
    if (changeFlags & QPinchGesture::ScaleFactorChanged) {
        double scale0 = m_Zoom ;
        qreal scale = m_Zoom*gesture->scaleFactor();

        QPointF c0 = S2C(screen);
        QPointF c1;

        c1 = c0 * (scale - scale0);

        if (gesture->scaleFactor() != gesture->lastScaleFactor()){
            m_offset += c1*-1.0;
            m_Zoom = scale;
            m_mousepos = screen;

            if(m_showMachine){
                m_machine_Zoom = m_Zoom;
                m_machine_Offset = m_offset;
            }
            else
            {
                m_normal_Zoom = m_Zoom;
                m_normal_Offset = m_offset;
            }
        }
    }

    update();
}


void DieCadViewer::panTriggered(QPanGesture *gesture)
{
QPointF delta;
#ifndef QT_NO_CURSOR
    switch (gesture->state()) {
        case Qt::GestureStarted:
        case Qt::GestureUpdated:
            setCursor(Qt::SizeAllCursor);
            delta = gesture->delta();
            if(!m_showMachine){

                m_offset.setX( m_offset.x() + delta.x());
                m_offset.setY( m_offset.y() - delta.y());

            }
            if(m_showMachine && m_isJobMoveAllowed){
                if(m_isRotationUsed) setRotation(m_rotationAngle);
                //m_zeroPoint.setX(m_zeroPoint.x() + (mapFromScene(delta).x())/m_Zoom);
                //m_zeroPoint.setY(m_zeroPoint.y() + (mapFromScene(delta).y())/m_Zoom);
                //ez is jó lehet de
                //m_zeroPoint.setX(m_zeroPoint.x() + (mapFromScene(gesture->offset()).x() - mapFromScene(gesture->lastOffset()).x())/m_Zoom);
                //m_zeroPoint.setY(m_zeroPoint.y() + (mapFromScene(gesture->offset()).y() - mapFromScene(gesture->lastOffset()).y())/m_Zoom);
                //talán inkább ez
                //qDebug() << "m_zeroPoint: " << m_zeroPoint << "delta: " << delta << "m_Zoom: " <<  m_Zoom << "(mapFromScene(gesture->offset()).x() - mapFromScene(gesture->lastOffset()).x())/m_Zoom:" << (mapFromScene(gesture->offset()).x() - mapFromScene(gesture->lastOffset()).x())/m_Zoom;

                bool  bDontMove = false;
                QPointF p = QPointF(m_zeroPoint.x() + delta.x()/m_Zoom,
                                    m_zeroPoint.y() + delta.y()/m_Zoom);

                if(   (m_machineRect.x() > p.x())
                    ||(m_machineRect.y() < p.y())
                    ||(m_machineRect.topRight().x() < (p.x() + m_jobRect.topRight().x()))
                    ||(fabs(m_machineRect.bottomRight().y()) < (fabs(p.y()) + m_jobRect.bottomRight().y())) )  bDontMove = true;

                if(! bDontMove) m_zeroPoint = p;

//                m_zeroPoint.setX(m_zeroPoint.x() + delta.x()/m_Zoom);
//                m_zeroPoint.setY(m_zeroPoint.y() + delta.y()/m_Zoom);
                if(m_isRotationUsed) setRotation(0);
                m_machine_Offset = m_offset;
                m_machine_ZeroPoint = m_zeroPoint;
                m_zeroPositionLastModified = QTime::currentTime();
                emit zeroPositionChanged();
            }
            else
            {
                m_normal_Offset = m_offset;
            }

            break;
        default:
            setCursor(Qt::ArrowCursor);
    }
    update();
#endif
}

void DieCadViewer::tapTriggered(QTapGesture *gesture)
{
    switch (gesture->state()) {
    case Qt::GestureStarted:
        ;
        break;
    case Qt::GestureFinished:
    {//simulates double click
        if(m_tapTime.elapsed() < m_tapDoubleClickTime )  {
            zoomAll();
            update();
        }
        m_tapTime.restart();
    }
        break;
    case Qt::GestureUpdated:
        ;
        break;
    case Qt::GestureCanceled:
        ;
        m_tapTime.restart();
        break;
    default:
        break;
    }
    return;
}



void DieCadViewer::timerEvent(QTimerEvent *)
{
    if(!m_isFileCheckedRegularly)
        return;

    if(m_isFileModified)
        checkFile();
    if(m_bNeedRefresh){
        m_isFileModified = false;
        m_bNeedRefresh = false;
        refresh();
    }
}



bool DieCadViewer::event(QEvent *event)
{
    //qDebug() << "DieCadViewer::event(QEvent *event)-- event->type(): " <<  event->type();
    if (event->type() == QEvent::Gesture)
        return gestureEvent(static_cast<QGestureEvent*>(event));
    return QDeclarativeItem::event(event);

}



//![1]
void DieCadViewer::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{

    if(!m_isLoaded) {
        if(m_isFileCheckedRegularly)
        {
           loadFile(m_nativefilename);
        }
    }


    //QPen pen(m_toolColors[0], 1);
    QPen pen(m_tools[0].color);
    //QFont f("ELCEDE DIN");
    QFont f("Roboto Bold");
    f.setPointSize(9);


    if(m_isLoaded){
        if (m_Zoom == 0 )zoomAll();
        //-- draw the length texts

        QString str;
/*
        QRectF r = QRectF(m_textMargin,
                          m_textMargin,
                          boundingRect().width()-2*m_textMargin,
                          boundingRect().height()-2*m_textMargin
                      );
*/
        pen = QPen(m_textColor);
        pen.setWidth(2);
        painter->setPen(pen);
        painter->setRenderHints(QPainter::TextAntialiasing, true);
        painter->setFont(f);
        QFontMetrics fm(f);

        painter->save();

        //--debug
/*
        str = QString("draw x: %1, draw y: %2, zoom: %3, m_offset.x: %4, y: %5, mouse.x: %6, mouse.y: %7, box.left: %8 bottom: %9").arg(
                                                      QString::number(m_Zoom,'f', 2),
                                                      QString::number(m_offset.x(),'f', 2),
                                                      QString::number(m_offset.y(),'f', 2),
                                                      QString::number(m_boundingBox.left(),'f', 2),
                                                      QString::number(m_boundingBox.bottom(),'f', 2));

        painter->drawText(r, Qt::AlignBottom | Qt::AlignHCenter, str);
*/
        //--debug

        if(m_isRotationUsed){
            painter->translate(m_rotationCenter);
            painter->rotate(m_rotationAngle);
            painter->translate(-m_rotationCenter);
        }
        painter->translate(m_offset.x(), -m_offset.y());
        painter->scale(m_Zoom, -m_Zoom);


        if(m_showMachine){
            painter->save();
            painter->translate(m_zeroPoint.x(), (m_machineRect.height() - m_zeroPoint.y()));
        }

        //qint64 before = QDateTime::currentDateTime().toMSecsSinceEpoch();

        //optimized method
        int toolIndex = 0;
        pen.setWidth(1);
        pen.setCosmetic(true);

        foreach ( const auto& elemIndex, m_elementPath.keys() )
        {

            toolIndex = m_elementPath.value(elemIndex).first;

            if( elemIndex == m_selectedElement)
            {
                pen.setColor(m_selectedColor);
                pen.setWidth(2);
                //                   qDebug() << "paint : -- m_selectedElement:" << m_selectedElement;
            }
            else if( elemIndex > m_actualElement)
            {
                //pen.setColor(m_toolColors[toolIndex]);
                //pen.setWidth(1);
                pen.setColor(m_tools[toolIndex].color);
                pen.setWidth(m_tools[toolIndex].width);
            }
            else
            {
                pen.setColor(m_processedColor);
                pen.setWidth(1);
            }
            pen.setStyle(Qt::SolidLine);
            painter->setPen(pen);

            if( m_toolShow.at(toolIndex) )
            {
                if(toolIndex == 0)
                {
                    pen.setStyle(Qt::DashLine);
                    painter->setPen(pen);
                }
                painter->drawPath(m_elementPath.value(elemIndex).second);
                //    qDebug() << "\t{" << toolKey << "," << "path is emty"  << "}";

            }
        }
        pen.setWidth(1);

        if(m_directionShow) //draw arrows showing direction
        {
            for(auto i = m_elements.begin(); i != m_elements.end(); ++i)
            {
                switch(i->second->getType()){
                case element::ET_TOOL:
                {
                    toolIndex = i->second->getInt(element::IID_TOOL);
                    break;
                }
                case element::ET_ARC:
                case element::ET_LINE:
                    toolIndex = i->second->getInt(element::IID_TOOL);
                    if(m_toolShow.at(toolIndex))
                    {
                        if(toolIndex == 0)
                            pen.setStyle(Qt::DashLine);
                        else
                            pen.setStyle(Qt::SolidLine);
                        //pen.setColor(m_toolColors[toolIndex]);
                        pen.setColor(m_tools[toolIndex].color);
                        pen.setCosmetic(true);
                        painter->setPen(pen);
                        drawArrow(i->second, painter);
                    }
                }
            }
        }

        //qDebug() << "after paint: " << QDateTime::currentDateTime().toMSecsSinceEpoch() - before;

        if(m_showMachine){
            painter->restore();
            painter->setPen(m_machinePen);
            painter->drawRect(m_machineRect);
            //qDebug() << "DieCadViewer plugin: -------- Machine rect is drawn! m_showFlange: " << m_showFlange;
            if(m_showFlange){
                pen = QPen(m_flangeColor );
                pen.setWidth(m_flangeWidth );
                painter->setPen(pen);
                bool bRotaMate = qFabs(m_machineRect.width()) < qFabs(m_machineRect.height());
                for(int i = 0; i <= 6; ++i){
                    if(bRotaMate){
                        painter->drawLine(m_machineRect.bottomLeft().x() + m_flangeWidth/2, m_machineRect.bottomLeft().y() + m_flangePosY[i],
                                          m_machineRect.bottomRight().x() - m_flangeWidth/2, m_machineRect.bottomRight().y() + m_flangePosY[i]);
                    }else{
                        painter->drawLine(m_machineRect.bottomLeft().x()   + m_flangePosY[i], m_machineRect.bottomLeft().y() + m_flangeWidth/2,
                                          m_machineRect.topLeft().x()   + m_flangePosY[i], m_machineRect.topLeft().y() - m_flangeWidth/2);
                    }
                }
            }
            drawMachinePosition(painter);
            //qDebug() << "-------paint----- m_machineRect:" << m_machineRect << "m_offset" << m_offset << "m_Zoom" << m_Zoom ;
        }


        painter->restore();

        qreal textWidth;
        qreal textHeight = fm.height();

        if(m_selectedElement > -1)
        {
            outStr(str); //format output string

            textWidth  = fm.width(str);

            QRectF textR = QRectF(QPointF(boundingRect().width()/2 - textWidth/2, boundingRect().height() - (m_textMargin + textHeight)),
                                  QPointF(boundingRect().width()/2 + textWidth/2,  boundingRect().height() - m_textMargin)
                            );
            painter->fillRect(textR, m_backgroundColor);
            painter->drawText(textR, Qt::AlignBottom | Qt::AlignHCenter, str);

        }

        str = QString("Min:%1 / %2 %3 - Max:%4 / %5 %3").arg(
                                     QString::number(m_min.x()  * m_movesFactor, 'f', 2),
                                     QString::number(m_min.y()  * m_movesFactor, 'f', 2),
                                     m_movesDimension,
                                     QString::number(m_max.x()  * m_movesFactor, 'f', 2),
                                     QString::number(m_max.y()  * m_movesFactor, 'f', 2));

        textWidth  = fm.width(str);

        QRectF textR = QRectF(QPointF(boundingRect().width()/2 - textWidth/2, m_textMargin),
                              QPointF(boundingRect().width()/2 + textWidth/2, m_textMargin + textHeight)
                        );
        painter->fillRect(textR, m_backgroundColor);
        painter->drawText(textR, Qt::AlignHCenter, str);

        str = QString("%1 %2%3 - %4 %5%3").arg(
                                m_totalMovesName,
                                QString::number(m_totalMoves  * m_movesFactor, 'f', 2),
                                m_movesDimension,
                                m_emptyMovesName,
                                QString::number(m_emptyMoves  * m_movesFactor, 'f', 2)
                    );


        textWidth  = fm.width(str);
        textR = QRectF(QPointF(boundingRect().width()/2 - textWidth/2, m_textMargin + textHeight),
                                      QPointF(boundingRect().width()/2 + textWidth/2, m_textMargin + textHeight*2)
                                );
        painter->fillRect(textR, m_backgroundColor);
        painter->drawText(textR, Qt::AlignHCenter, str);


        //drawPoint(painter);
    }
    else
    {
        QPen pen(m_textColor);
        pen.setWidth(2);
        painter->setPen(pen);
        painter->setRenderHints(QPainter::Antialiasing, true);
        painter->setFont(f);
       // QRectF r = boundingRect().adjusted(2,2,-2,-2);
        QRectF r = QRectF(m_textMargin,
                          m_textMargin,
                          boundingRect().width()-2*m_textMargin,
                          boundingRect().height()-2*m_textMargin
                          );

        painter->drawRect(r);

        painter->drawText(r,m_nativefilename);
        f.setStyleStrategy( QFont::ForceOutline);
        f.setPointSize(24);
        painter->setFont(f);
        painter->drawText(r, Qt::AlignCenter, m_fileErrorText);
    }

    if(m_bStartExecuteCmd)
    {
        executeCmd();
        m_bStartExecuteCmd = false;
    }

}

QColor DieCadViewer::toolColor() const
{
    //auto ii = m_toolColors.find(m_toolIndex);
    auto ii = m_tools.find(m_toolIndex);
    //if(ii != m_toolColors.end())
    if(ii != m_tools.end())
        return ii.value().color;
    else
        return QColor();
}


void DieCadViewer::setToolColor(const QColor &color)
{
    //m_toolColors[m_toolIndex] = color;
    m_tools[m_toolIndex].color = color;
    update();

}

int DieCadViewer::toolWidth() const
{
    auto ii = m_tools.find(m_toolIndex);
    if(ii != m_tools.end())
        return ii.value().width;
    else
        return 1;
}


void DieCadViewer::setToolWidth(const int &w)
{
    m_tools[m_toolIndex].width = w;
    update();
}

qreal DieCadViewer::toolPathLength() const
{
    auto ii = m_tools.find(m_toolIndex);
    if(ii != m_tools.end())
        return ii.value().pathLength;
    else
        return 0;
}

bool DieCadViewer::toolShow() const
{
    return m_toolShow.at(m_toolIndex);
}

void DieCadViewer::setToolShow(const bool show)
{
   m_toolShow[m_toolIndex] = show;
   update();
}

int DieCadViewer::toolIndex() const
{
    return m_toolIndex;
}

void DieCadViewer::setToolIndex(int i)
{
    if(i >= 0 && i <  41 )
        m_toolIndex = i;
    else
        m_toolIndex = 0;

}

bool DieCadViewer::directionShow() const
{
    return m_directionShow;
}

void DieCadViewer::setDirectionShow(const bool bShow)
{
    m_directionShow = bShow;
    update();
}



//![1]

//![2]

void DieCadViewer::loadFile(QString fileName)
{
    m_IsEngraving = false;

    m_isLoaded = false;

    m_boundingBox.setSize(QSizeF(0,0));
    m_boundingBox.translate(QPointF(0,0));

    m_emptyMoves = 0.0;
    m_totalMoves = 0.0;

    m_offset = QPointF(0,0);
    m_mousepos = QPointF(0,0);

    m_selectedElement = -1;

    m_numofElem = 0;

    if (m_elements.size())
        for (std::map< int, element* >::iterator it = m_elements.begin(); it != m_elements.end(); ++it){
            element*		E = it->second;
            delete E;
        }

    m_elements.clear();
    m_elementPath.clear();

    for(int i = 0; i < 41; ++i)
    {
        m_tools[i].pathLength = 0.0;
    }


    QFileInfo check_file(fileName);
    // check if path exists and if yes: Is it really a file and no directory?
    if( ! (check_file.exists() && check_file.isFile()) ){
        if(!fileName.isEmpty())
            qDebug() << "File doesn't exist:" << fileName ;
        return;
    }

    qDebug() << "Loading file:" << fileName ;
    if(fileName.endsWith(".cnc", Qt::CaseInsensitive)){
        fileIO::cnc_file file(m_elements);
        file.Load(fileName);
    }else if (fileName.endsWith(".acc", Qt::CaseInsensitive)) {
        fileIO::acc_file file(m_elements);
        file.Load(fileName);
    }else if(fileName.endsWith(".plt", Qt::CaseInsensitive)) {
        fileIO::hpgl_file file(m_elements);
        file.Load(fileName);
        scalePLTfile();
    }

    QPainterPath gPath = QPainterPath();

    if(m_elements.size()) {

        //QPainterPath        path =  QPainterPath();
        m_numofElem = m_elements.size();
        Die::Coord          c;
        int                 toolNum = 0;

        m_isLoaded = true;
        for(auto i = m_elements.begin(); i != m_elements.end(); ++i)
        {
            switch(i->second->getType())
            {
            case element::ET_TOOL:
            {
            //    if(!path.isEmpty()) {
            //        m_boundingBox = m_boundingBox.united(path.boundingRect());
            //        path =  QPainterPath();
            //    }
                if(toolNum != i->second->getInt(element::IID_TOOL)){
                    toolNum = i->second->getInt(element::IID_TOOL);
                    if(!m_IsEngraving && (toolNum == m_engravingTool)) m_IsEngraving = true;
                }

            }
                break;
            case element::ET_LINE:
            {
                QPainterPath        path =  QPainterPath();
                if(toolNum != i->second->getInt(element::IID_TOOL)){
                    toolNum = i->second->getInt(element::IID_TOOL);
                }

                m_totalMoves += i->second->getLength();
                if( toolNum == 0) m_emptyMoves += i->second->getLength();
                //TODO: Macro(32772(1,-3.74053687)) is used for the lifetime; should it be used for the toolPathLength???
                //update toolPathLenght
                m_tools[toolNum].pathLength += i->second->getLength();

                int elementID = i->second->getInt(element::IID_ID);

                c = i->second->getCoord(element::CID_START);
                path.moveTo(QPointF(c.x, c.y));
                c = i->second->getCoord(element::CID_END);
                path.lineTo(QPointF(c.x, c.y));

                m_elementPath.insert(elementID, qMakePair(toolNum, path ));

                gPath.addPath(path);
            }
                break;
            case element::ET_ARC:
            {
                QPainterPath        path =  QPainterPath();
                bool		_todraw = false;
                int elementID = i->second->getInt(element::IID_ID);

                if(toolNum != i->second->getInt(element::IID_TOOL)){
                    toolNum = i->second->getInt(element::IID_TOOL);

                }

                Die::Coord sp = i->second->getCoord(element::CID_START);
                Die::Coord ep = i->second->getCoord(element::CID_END);
                Die::Coord cp = i->second->getCoord(element::CID_CENTER);
                double angle  = i->second->getDouble(element::DID_ARCANGLE);

                double radius = (sp - cp).norm();

                if ((sp.x != ep.x) || (sp.y != ep.y)) _todraw = true;
                else {
                     if ((sp - ep).norm() < 0.01) _todraw = true;
                }
                if (_todraw) {
                    path.moveTo(sp.x, sp.y);
                    //Arc(sp, cp, ep, radius, angle > 0 ? false:true , path);

                    Die::Angle StA(sp - cp);
                    double startAngle;

                    startAngle = (StA.degree() + angle)*-1;

                    path.arcMoveTo(cp.x - radius, cp.y - radius, 2*radius, 2*radius, startAngle);
                    path.arcTo(cp.x - radius, cp.y - radius, 2*radius, 2*radius, startAngle, angle);

                    m_totalMoves += i->second->getLength();
                    if( toolNum == 0) m_emptyMoves += i->second->getLength();
                    //update toolPathLenght
                    m_tools[toolNum].pathLength += i->second->getLength();

                }

                m_elementPath.insert(elementID, qMakePair(toolNum, path ));

                gPath.addPath(path);
            }
                break;
                /*
            case element::ET_MACRO:
            {
                qDebug() << "MACRO:" << i->second->getString(element::SID_MACRO);
            }
                break;
                */
            }


        }

        //qDebug() << "m_boundingBox: " << m_boundingBox << "path.isEmpty():" << path.isEmpty();

        //if(!path.isEmpty()) {
            //m_boundingBox = m_boundingBox.united(path.boundingRect());
            m_boundingBox = m_boundingBox.united(gPath.boundingRect());
            m_min.setX(m_boundingBox.left());
            m_min.setY(m_boundingBox.top());
            m_max.setX(m_boundingBox.right());
            m_max.setY(m_boundingBox.bottom());
            m_jobRect = QRectF(m_min, m_max);

         //   qDebug() << "m_max: " << m_max;
         //}

        m_boundingBox.adjust(-m_boundingBox.width()/100, -m_boundingBox.height()/100, m_boundingBox.width()/100, m_boundingBox.height()/100);



    }

    //qDebug() << "m_elements.size():" << m_elements.size() << "gPath.elementCount():" << gPath.elementCount();

    zoomAll();

    emit fileLoaded();
}


void DieCadViewer::refresh()
{
/*
    QString fileName = m_filename.mid(6,-1);

    QUrl url = QUrl::fromLocalFile( m_filename.mid(6,-1));

    fileName = convertUrlToNativeFilePath(url);
*/
    m_isLoaded = false;

    //loadFile(fileName);
    loadFile(m_nativefilename);

    update();
}

void DieCadViewer::reDraw()
{
    zoomAll();
    update();
}


void DieCadViewer::drawArrow(element *pElem, QPainter *painter)
{

    qreal scale = 0.08 / m_Zoom;

    //az end-re kell tenni, hogy ua. legyen mint Aradex Contour editor
    Die::Coord ep = pElem->getCoord(element::CID_END);
    Die::Coord p;
    Die::Angle endAngle;

    if(pElem->getType() == element::ET_LINE){
        p = pElem->getCoord(element::CID_START);
        endAngle = Die::Angle(p -ep) + Die::Angle(Die::Rad(Die_pi));
    }else{

        p = pElem->getCoord(element::CID_CENTER);
        if(pElem->getDouble(element::DID_ARCANGLE) > 0)
            endAngle = Die::Angle(ep - p) + Die::Angle(Die::Rad(Die_pi2));
        else
            endAngle = Die::Angle(ep - p) - Die::Angle(Die::Rad(Die_pi2));
    }

    painter->save();
    painter->translate(ep.x, ep.y);
    painter->rotate(endAngle.degree());//+180);
    painter->scale(scale, scale);
    //painter->drawPolygon(m_arrowPoints, 3);
    painter->drawPath(m_arrowDirection);
    painter->restore();

}


void DieCadViewer::Arc(element* pElem, QPainter* p )
{
    Die::Coord sp = pElem->getCoord(element::CID_START);
    Die::Coord cp = pElem->getCoord(element::CID_CENTER);
    double angle  = pElem->getDouble(element::DID_ARCANGLE);

    double radius = (sp - cp).norm();

    Die::Angle StA(sp - cp);
    double startAngle;
    if( angle > 0 )
    {
        startAngle = StA.degree() * -1 - 45;
    }
    else
    {
        startAngle = StA.degree() - 45;
    }

    p->drawArc(cp.x - radius, cp.y - radius, 2*radius, 2*radius, startAngle*16, angle*16);

}
/*
//select element by id
int DieCadViewer::selectElem(int id)
{
    m_selectedElement = -1;
    for(auto i = m_elements.begin(); i!= m_elements.end(); ++i )
    {
        int IID_ID = i->second->getInt(element::IID_ID);
        if(IID_ID == id)
        {
            m_selectedElement = i->first;
            return m_selectedElement;
        }
    }

    //qDebug() << "m_selectedElement:" << m_selectedElement;
    update();
    return m_selectedElement;
}
*/

void DieCadViewer::selectElem(QPointF &mouseP)
{

    double minDist = 5.0 * m_Zoom;

    m_selectedElement = -1;
    for(auto i = m_elements.begin(); i!= m_elements.end(); ++i )
    {
        int type = i->second->getType();
        if(type == element::ET_LINE || type == element::ET_ARC){
            int toolIndex = i->second->getInt(element::IID_TOOL);
            //qDebug() << "toolIndex: " << toolIndex << " -- " << m_toolShow.at(toolIndex);
            if(m_toolShow.at(toolIndex)){

                double tmpDist = i->second->getDistance(Die::Coord(mouseP.x(), mouseP.y()));

                if(minDist > tmpDist )
                {
                    minDist = tmpDist;
                    m_selectedElement = i->first;

                }
            }
        }
    }

    //qDebug() << "m_selectedElement:" << m_selectedElement;
    update();

}



void DieCadViewer::outStr(QString &str)
{
    Die::Coord end, center;
    double z;
    double angle;
    if((m_selectedElement > -1) && (m_selectedElement < int(m_elements.size()))){

        auto elem = m_elements.at(m_selectedElement);
        if(elem){

            switch(elem->getType())
            {
            case element::ET_LINE:
                end = elem->getCoord(element::CID_END)/1000;
                z = elem->getDouble(element::DID_ZCOORD)/1000;
                str = QString("[%1] Id = %2 Line(XYZ, %3, %4, %5)").arg(
                            QString::number(elem->getInt(element::IID_LINENUM)),
                            QString::number(elem->getInt(element::IID_ID)),
                            QString::number(end.x, 'f', 6),
                            QString::number(end.y, 'f', 6),
                            QString::number(z, 'f', 6)
                            );
                break;
            case element::ET_ARC:
                center = elem->getCoord(element::CID_CENTER)/1000;
                z = elem->getDouble(element::DID_ZCOORD)/1000;
                angle = elem->getDouble(element::DID_ARCANGLE);
                str = QString("[%1] Id = %2 Arc(XYZ, %3, %4, %5, 0, 0, 1, %6)").arg(
                            QString::number(elem->getInt(element::IID_LINENUM)),
                            QString::number(elem->getInt(element::IID_ID)),
                            QString::number(center.x, 'f', 6),
                            QString::number(center.y, 'f', 6),
                            QString::number(z, 'f', 6),
                            QString::number(angle, 'f', 14)
                            );
                break;
            case element::ET_TOOL:
                str = QString("[%1] Id = %2 Tool(%3)").arg(
                            QString::number(elem->getInt(element::IID_LINENUM)),
                            QString::number(elem->getInt(element::IID_ID)),
                            QString::number(elem->getInt(element::IID_TOOL))
                            );
                break;
            case element::ET_MACRO:
                str = QString("[%1] Id = %2 Macro(%3)").arg(
                            QString::number(elem->getInt(element::IID_LINENUM)),
                            QString::number(elem->getInt(element::IID_ID)),
                            QString::number(elem->getInt(element::SID_MACRO))
                            );
                break;
            }

        }else{
            qDebug() << "Element Id= " << m_selectedElement << " not found!";
        }
    }
}

QPointF DieCadViewer::S2C(const QPointF &p)
{
    QPointF sP;
    QPointF pt = p;
    if(m_isRotationUsed){
        QPointF pt1 = p - m_rotationCenter;
        QPointF pt2 = QPointF(pt1.y(),pt1.x()*-1);//-90 fok
        pt = pt2 + m_rotationCenter;
    }
    sP = QPointF(pt.x() - m_offset.x(), -1.0*(pt.y() + m_offset.y()))/m_Zoom;
    return sP;
}

void DieCadViewer::zoomAll()
{
    if( m_boundingBox.width() == 0) return;
    if( m_boundingBox.height() == 0) return;

    qreal zoomX;
    qreal zoomY;
    qreal zoom;
    qreal offsetx;
    qreal offsety;

    qreal area = (100 - m_machineMargin) /100.0;
    //m_rotationAngle = 0;

    if(!m_showMachine){
        if(m_isRotationUsed){
            zoomX   = boundingRect().width() / m_boundingBox.height();
            zoomY   = boundingRect().height() / m_boundingBox.width();
            zoom    = zoomX > zoomY ? zoomY : zoomX;
            zoom *=area;

            offsetx = (boundingRect().width() - m_boundingBox.width() * zoom) / 2.0;
            double height = m_boundingBox.height() * zoom;
            offsety = - height - (boundingRect().height() - height) / 2.0;

            m_offset = QPointF(offsetx, offsety);
            m_rotationCenter = QPointF(boundingRect().width()/2, boundingRect().height()/2);
        }
        else
        {
            zoomX   = boundingRect().width() / m_boundingBox.width();
            zoomY   = boundingRect().height() / m_boundingBox.height();
            zoom    = zoomX > zoomY ? zoomY : zoomX;
            zoom *= area;

            offsetx =( m_boundingBox.left() + m_boundingBox.width() / 2.0)*zoom;
            offsety =( m_boundingBox.top()+ m_boundingBox.height()/2.0) *zoom*-1.0 - boundingRect().height() /2.0;
            m_offset.setX( (boundingRect().width() / 2.0) - offsetx);
            m_offset.setY( offsety );
        }
    }
    else
    {
        if(m_isRotationUsed){  // show machine rect
            zoomX   = boundingRect().width() / - m_machineRect.height();
            zoomY   = boundingRect().height() / m_machineRect.width() ;
            zoom    = zoomX > zoomY ? zoomY : zoomX;
            zoom *= area;

            offsetx = (boundingRect().width() - m_machineRect.width() * zoom) /2.0;
            double height = m_machineRect.height() * zoom;
            offsety = - height - (boundingRect().height() - height) / 2.0;

            m_offset = QPointF(offsetx, offsety);
            m_rotationCenter = QPointF(boundingRect().width()/2, boundingRect().height()/2);


        }
        else{
            zoomX   = boundingRect().width() / m_machineRect.width();
            zoomY   = boundingRect().height() / - m_machineRect.height() ;
            zoom    = zoomX > zoomY ? zoomY : zoomX;
            zoom *= area;
            offsetx =( m_machineRect.left() + m_machineRect.width() / 2.0)*zoom;
            offsety =( m_machineRect.top()+ m_machineRect.height()/2.0) *zoom*-1.0 - boundingRect().height() /2.0;
            m_offset.setX( (boundingRect().width() / 2.0) - offsetx);
            m_offset.setY( offsety );

        }
    }

    m_Zoom        = zoom;

    if(m_showMachine){
        m_machine_Zoom = m_Zoom;
        m_machine_Offset = m_offset;
    }
    else
    {
        m_normal_Zoom = m_Zoom;
        m_normal_Offset = m_offset;
    }
    //qDebug() << "boundingRect() " << boundingRect() << " m_rotationCenter " << m_rotationCenter << " m_offset " << m_offset << " m_Zoom " << m_Zoom << "m_machineRect" << m_machineRect << "m_boundingBox" << m_boundingBox;
}

bool DieCadViewer::fileCopy(QString from, QString to)
{
    if(QFile::exists(to)) {
        if (!QFile::remove(to)){
            qDebug() << "fileCopy failed -- file cannot be removed:" << to;
            return false;
        }
    }
    if(QFile::exists(from)){
        if (QFile::copy(from, to)){
            return true;
        }
        else
        {
            qDebug() << "fileCopy failed ";
            return false;
        }
    }
    else
    {
        qDebug() << "fileCopy failed -- file:" << from << "does not exist!";
        return false;
    }
}

QString DieCadViewer::fileOpen(QObject *Q)
{
    QStringList filters;

   // filters << "CAD files (*.acc *.cnc *.plt, *.hpg)"
   //         << "Any files (*)";

    filters << m_fileOpenFilter
            << "Any files (*)";


    //QFileDialog dlg(qobject_cast<QWidget *>(this));
    QFileDialog dlg(qobject_cast<QWidget *>(Q), Qt::WindowStaysOnTopHint);
    dlg.setNameFilters(filters);
    dlg.setDirectory(m_fileOpenDirectory);
    dlg.selectFile(m_fileOpenSelectFile);
    dlg.setFileMode(QFileDialog::ExistingFile);
    dlg.setModal(true);

    if(dlg.exec() == QDialog::Accepted)
    {
        //qDebug() << "-------dlg. Selected file -------- " << dlg.selectedFiles().at(0);
        return dlg.selectedFiles().at(0);
    }
    else
        //qDebug() << "-------dlg. rejected -------- ";
    return "";

}

void DieCadViewer::checkFile()
{
    FILE *pFile = NULL;
    long lSize;
    char *buffer;
    size_t result;

    if (! _wfopen_s(&pFile, m_nativefilename.toStdWString().c_str(), L"rb")==0) {
        m_bNeedRefresh = true;
        return;
    }


    fseek(pFile,0,SEEK_END);
    lSize = ftell(pFile);
    rewind (pFile);

    int num = 100;
    if( lSize < num) num = lSize;

    buffer = (char*) malloc (sizeof(char)*num);
    if (buffer == NULL)  {  fclose(pFile); return;}
    result = fread (buffer,1,num,pFile);

    if (result != num)  {  fclose(pFile); return;}

    qint16 begin = qChecksum(buffer, num);

    bool isBeginSame = false;
    bool isEndSame = false;


    if( begin != m_cheksumBegin ){
        m_cheksumBegin = begin;
        m_isFileChanged = true;
    }else
        isBeginSame = true;

    rewind (pFile);

    if(lSize > num){
        fseek ( pFile , lSize - num , SEEK_SET );
        result = fread (buffer,1,num,pFile);
        if (result != num){  fclose(pFile); return;}

        qint16 end = qChecksum(buffer, num);
        if( end != m_cheksumEnd){
            m_cheksumEnd = end;
            m_isFileChanged = true;
        }else
            isEndSame = true;

    }

    fclose(pFile);
    free(buffer);


    if( isBeginSame && isEndSame && m_isFileChanged){
        m_isFileChanged = false;
        m_bNeedRefresh = true;
    }else
        m_bNeedRefresh = false;

}


void DieCadViewer::drawMachinePosition(QPainter *p)
{
    p->setPen(m_machinePen);
    QPointF point = QPointF(m_machinePosition.x(), m_machinePosition.y() + m_machineRect.height()); //- m_zeroPoint;
    p->drawPath(m_machineHead.translated(point));
}

void DieCadViewer::scalePLTfile()
{
    for (auto it = m_elements.begin(); it != m_elements.end(); ++it) {
        if (it->second->getType() == element::ET_LINE || it->second->getType() == element::ET_ARC) {

            Die::Coord _sp = (*it).second->getCoord(element::CID_START) * 0.025;
            Die::Coord _ep = (*it).second->getCoord(element::CID_END) * 0.025;
            Die::Coord _cp;

            if ((*it).second->getType() == element::ET_ARC) {
                _cp = (*it).second->getCoord(element::CID_CENTER) * 0.025;
                (*it).second->setCoord(element::CID_CENTER, _cp);
            }

            (*it).second->setCoord(element::CID_START, _sp);
            (*it).second->setCoord(element::CID_END, _ep);
            (*it).second->setLength(calcLength(it->second->getType(), _sp, _ep, _cp));

        }
    }
}


double DieCadViewer::calcLength(int elemType, Die::Coord _sp, Die::Coord _ep, Die::Coord _cp)
{

    double length = 0.0;

    //                            double x1, x2, y1, y2, m1, m2, degree;
    double radius;

    if (elemType == element::ET_LINE) {
        length = sqrt(((_sp.x - _ep.x)*(_sp.x - _ep.x)) + ((_sp.y - _ep.y)*(_sp.y - _ep.y)));

    }
    else if (elemType == element::ET_ARC) {

        Die::FreeAngle SwA;
        Die::Angle StA(_sp - _cp);
        Die::Angle EnA(_ep - _cp);

        if (math::closeto(StA.degree(), EnA.degree(), math::getTolerance()))
            SwA = Die::Rad(Die_2pi);
        else
            if (StA.radian() < EnA.radian())
                SwA = EnA - StA;
            else
                SwA = Die::Rad(Die_2pi) - (StA - EnA);


        radius = (_sp - _cp).norm();

        length = radius * SwA.radian();
    }

    return length;
}

bool DieCadViewer::executeCmd()
{
    m_process_ptr = new QProcess(this);  //process_ptr is declared in my header file QProcess *process_ptr;
    if(QFile::exists(m_strExecuteCmd))
    {
        QStringList params = m_strExecuteCmdParameters.split(";");
        m_process_ptr->start(m_strExecuteCmd, params);
        qDebug() << "Starting command: " << m_strExecuteCmd << m_strExecuteCmdParameters;

        connect(m_process_ptr , SIGNAL(started()), this, SLOT(executeCmdStarted()));
        connect(m_process_ptr , SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(executeCmdEnd(int, QProcess::ExitStatus)));
        return true;
    }
    else
    {
        qDebug() << "File does not exist: " << m_strExecuteCmd;
    }
    return false;
}

void DieCadViewer::executeCmdStarted()
{
    qDebug() << "command started: " << m_strExecuteCmd << m_strExecuteCmdParameters;
}

void DieCadViewer::executeCmdEnd(int iExitCode, QProcess::ExitStatus exitStatus)
{
    qDebug() << "iExitCode" << iExitCode << "exitStatus" << exitStatus;
    m_process_ptr = NULL;
    emit executeCmdFinished();
}


//![2]
