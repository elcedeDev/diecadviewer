#pragma once
#include "die_coord.h"
#include "Import.h"


#define 	MAKE_HPGL_CMD(a, b)   (((int)(a) << 8) + (b))

#define 	HPGL_IN		MAKE_HPGL_CMD('I','N') //Initialize Set Instruction
//Pen Control and Plot Instructions
#define 	HPGL_AA		16640 + 65 // MAKE_HPGL_CMD('A','A') //ARC ABSOLUTE
#define 	HPGL_AR		MAKE_HPGL_CMD('A','R') //ARC RELATIVE
#define 	HPGL_PU		MAKE_HPGL_CMD('P','U') //PEN UP
#define 	HPGL_PD		MAKE_HPGL_CMD('P','D') //PEN DOWN
#define 	HPGL_PA		MAKE_HPGL_CMD('P','A') //Plot Absolute
#define 	HPGL_PR		MAKE_HPGL_CMD('P','R') //Relative Coordinate Pen Move
#define 	HPGL_CI		MAKE_HPGL_CMD('C','I') //CIRCLE
#define		HPGL_SP		MAKE_HPGL_CMD('S','P') //select pen

#define 	HPGL_DI		MAKE_HPGL_CMD('D','I') //ABSOLUTE DIRECTION  see label

#define 	HPGL_AC		MAKE_HPGL_CMD('A','C') //
#define 	HPGL_AD		MAKE_HPGL_CMD('A','D')
//Plot area and unit setting instructions
#define 	HPGL_IP		MAKE_HPGL_CMD('I','P') //Scaling point
#define 	HPGL_SC		MAKE_HPGL_CMD('S','C') //Scale
#define 	HPGL_RO		MAKE_HPGL_CMD('R','O') //Rotate coordinate system

//#define _pi2		1.57079632679489661922
//#define _pi			3.14159265358979323844
//#define _2pi		6.28318530717958647688

namespace fileIO
{
	class hpgl_file : public file
	{

		enum pen_status{PEN_UP, PEN_DOWN};
		enum pen_move{PEN_ABSOLUTE, PEN_RELATIVE};
	
		char			*m_data;			//the whole unput file contents

		int				m_last_cmd;			//stores the last command
		int				m_param_counter;	//parameter counter to find out x or y or angle

		bool			m_bMirrorX;
		bool			m_bMirrorY;

		element			*m_E;

		Die::Coord		m_start, m_end, m_center;
		double			m_angle;
		_locale_t		L;
		struct Pen
		{
			int	number;
			int status;
			int move;
			double x, y;
		};

	public:
		hpgl_file( std::map< int, element* >& , bool = true);
		~hpgl_file();
		virtual bool canLoad() { return true; }
        virtual void Load( const QString& fileName );

	private:
		int	read_hpgl(FILE*);

		
		int process_command(int command );
		int process_param(std::string &s );
		void setPenCoords(double dcoord);
        void load_element(int etype);

		double calcLength(int elemType, Die::Coord _sp, Die::Coord _ep, Die::Coord _cp, double angle);
		

        Pen m_pen;
		bool m_isLoading;
	};

}
